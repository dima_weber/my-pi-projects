#ifndef QWBRLCD_H
#define QWBRLCD_H

#include <wiringPi.h>
#include <lcd.h>
#include <QObject>

class QTimer;
class QString;
class LcdDriver : public QObject
{
    Q_OBJECT
        int handle;
        QTimer** scrollTimers;
        QString* scrollTexts;
        int*     scrollPos;
        int cols, rows;
    public:
        LcdDriver(int cols, int rows, QObject* parent =0);
        ~LcdDriver(); 

    public slots:
        void home() { lcdHome(handle); }
        void clear(){ lcdClear(handle); }
        void display(bool state) {lcdDisplay (handle, state); }
        void cursor (bool state) {lcdCursor  (handle, state); }
        void cursorBlink (bool state) {lcdCursorBlink(handle, state); }
        void sendCommand (unsigned char command) {lcdSendCommand(handle, command); }
        void setPosition (int col, int row) {lcdPosition(handle, col, row); }
        void charDef (int index, unsigned char data[8]) {lcdCharDef (handle, index, data); }
        void putchar (unsigned char data) {lcdPutchar(handle, data); }
        void puts (const char* str) {lcdPuts(handle, str);}

        void display(int col, int row, const QString& text) { pauseScroll(row), setPosition(col, row); puts(text.toUtf8().constData());}

        void scrollLine(int row, const QString& str, int delay = 1000, int wsCnt = 3);
        void pauseScroll(int row);
        void resumeScroll(int row);
    protected slots:
        void onScrollTimer();
};
#endif
