#include "emailengine.h"
#include "config.h"
#include <qxtsmtp.h>
#include <qxtmailmessage.h>
#include <QCoreApplication>
#include <QTimer>

void SMTPThread::setupSignals()
{
    connect(p_smtp, SIGNAL(authenticated()), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(authenticationFailed()),this,  SLOT(onSMTPDisconnected()));
    connect(p_smtp, SIGNAL(authenticationFailed ( const QByteArray &)), this, SLOT(smtpError(QByteArray)));
    connect(p_smtp, SIGNAL(connected()), this, SLOT(onSMTPConnected()));
    //connect(p_smtp, SIGNAL(connectionFailed()), this, SLOT(onSMTPDisconnected()));
    connect(p_smtp, SIGNAL(connectionFailed ( const QByteArray &)), this, SLOT(smtpError(QByteArray)));
    connect(p_smtp, SIGNAL(disconnected()), this, SLOT(onSMTPDisconnected()));

    //connect(p_smtp, SIGNAL(encrypted ()), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(encryptionFailed ()), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(encryptionFailed ( const QByteArray & )), this, SLOT(smtpError(QByteArray)));
    //connect(p_smtp, SIGNAL(finished ()), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(mailFailed ( int , int )), this, SLOT(dummySlot()));
    connect(p_smtp, SIGNAL(mailFailed ( int , int , const QByteArray & )), this, SLOT(onMailFail(int,int,const QByteArray&)));
    connect(p_smtp, SIGNAL(mailSent ( int)), this, SLOT(onMailSent(int)));
    //connect(p_smtp, SIGNAL(recipientRejected ( int, const QString & )), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(recipientRejected ( int, const QString & , const QByteArray & )), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(senderRejected ( int, const QString & )), this, SLOT(dummySlot()));
    //connect(p_smtp, SIGNAL(senderRejected ( int, const QString &, const QByteArray & )), this, SLOT(dummySlot()));
}

void SMTPThread::readConfig(const QString& group)
{
    Config config;

    config.beginGroup(group);
    server = config.sValue("host", "smtp.example.com");
    user   = config.sValue("user", "user");
    password = config.sValue("password", "password");
    port = config.iValue("port", 443);
    useSsl = config.bValue("use_ssl", true);
    useTls = config.bValue("use_tls", true);
    config.endGroup();

    config.beginGroup("email1");
    subject = config.sValue("subject", "rpiservice email");
    recipient = config.sValue("recipient", "nobody@example.com");
    sender = config.sValue("sender", "nobody@example.com");
    config.endGroup();
}

void SMTPThread::initialize()
{
    connected = false;
    p_smtp= new QxtSmtp(this);

    setupSignals();
}

SMTPThread::SMTPThread(const QString& emailName, QObject *parent)
    : QThread(parent)
{
    initialize();
    readConfig(emailName);
}

SMTPThread::SMTPThread(const QString& user, const QString& password, const QString& server, int port, bool tls, bool useSsl, QObject* parent)
    : QThread(parent), user(user), password(password), server(server), port(port), useTls(tls), useSsl(useSsl)
{
    initialize();
}

   
void SMTPThread::connectToServer()
{
    QByteArray login = user.toUtf8();
    p_smtp->setUsername(login);
    QByteArray pwd = password.toUtf8();
    p_smtp->setPassword(pwd);
    p_smtp->setStartTlsDisabled(!useTls);
#ifndef QT_NO_OPENSSL
    if(useSsl)
        p_smtp->connectToSecureHost(server, port);
    else
#endif
        p_smtp->connectToHost(server, port);
}


SMTPThread::~SMTPThread ()
{
    p_smtp->disconnectFromHost();
    delete p_smtp;
}

void SMTPThread::run ()
{
//    qDebug() << "Email logger thread started";
    QTimer timer;
    connect(&timer, SIGNAL(timeout()), SLOT(mailBuffer()));
    timer.start (60000);
    exec();
//    qDebug() << "Email logger thread stopped";
}

void SMTPThread::stop (bool waitForFinished)
{
    quit();
    if (waitForFinished)
        wait();
}

void SMTPThread::addMsg (QString msg)
{
    msgBuffAccessMutex.lock();
    msgBuff << msg;
    msgBuffAccessMutex.unlock();
}

void SMTPThread::mailBuffer ()
{
    if (msgBuff.isEmpty ())
        return;
    if (!connected)
    {
	connectToServer();
	return;
    }

    QxtMailMessage msg;

    msgBuffAccessMutex.lock();
    msg.setBody(msgBuff.join("\n"));
    msgBuff.clear ();
    msgBuffAccessMutex.unlock();

    msg.setSubject(subject);
    msg.addRecipient(recipient);
    msg.setSender(sender);

    p_smtp->send(msg);
}

void SMTPThread::onSMTPConnected()
{
    qDebug() << "SMTP connected";
    connected = true;
    mailBuffer();
}

void SMTPThread::onSMTPDisconnected()
{
    qDebug() << "SMTP disconnected";
    connected = false;
}

void SMTPThread::dummySlot()
{
    qDebug() << QString("smtp auth ok");
}

void SMTPThread::smtpError(QByteArray msg)
{
    qDebug() << QString("smtp error: %1").arg(msg.data());
}

void SMTPThread::onMailSent(int id)
{
    qDebug() << QString("mail with id %1 sent.").arg(id);
    p_smtp->disconnectFromHost();
}

void SMTPThread::onMailFail(int mailid, int errCode, const QByteArray& msg)
{
    qDebug() << QString("fail to send mail %1: [%2] %3").arg(mailid).arg(errCode).arg(QString(msg));
    p_smtp->disconnectFromHost();
}
