#include "bmpsensor.h"
#include <QDebug>

#include <stdio.h>
#include <stdint.h>
#include <fcntl.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>
#include <sys/ioctl.h>
#include "smbus.h"

#define BMP085_I2C_ADDRESS 0x77

const unsigned char BMP085_OVERSAMPLING_SETTING = 3;

// Open a connection to the bmp085
// Returns a file id
int BmpSensor::bmp085_i2c_Begin()
{
   int fd;

   // Open port for reading and writing
   if ((fd = open(deviceFileName.toUtf8().constData(), O_RDWR)) < 0)
      throw openError;

   // Set the port options and set the address of the device
   if (ioctl(fd, I2C_SLAVE, BMP085_I2C_ADDRESS) < 0) {
      close(fd);
      throw ioctlError;
   }

   return fd;
}

// Read two words from the BMP085 and supply it as a 16 bit integer
__s32 BmpSensor::bmp085_i2c_Read_Int(int fd, __u8 address)
{
   __s32 res = i2c_smbus_read_word_data(fd, address);
   if (res < 0) {
      close(fd);
      throw readError;
   }

   // Convert result to 16 bits and swap bytes
   res = ((res<<8) & 0xFF00) | ((res>>8) & 0xFF);

   return res;
}

//Write a byte to the BMP085
void BmpSensor::bmp085_i2c_Write_Byte(int fd, __u8 address, __u8 value)
{
   if (i2c_smbus_write_byte_data(fd, address, value) < 0) {
      close(fd);
      throw writeError;
   }
}

// Read a block of data BMP085
void BmpSensor::bmp085_i2c_Read_Block(int fd, __u8 address, __u8 length, __u8 *values)
{
   if(i2c_smbus_read_i2c_block_data(fd, address,length,values)<0) {
      close(fd);
      throw readError;
   }
}

void BmpSensor::bmp085_Calibration()
{
   int fd = bmp085_i2c_Begin();
   ac1 = bmp085_i2c_Read_Int(fd,0xAA);
   ac2 = bmp085_i2c_Read_Int(fd,0xAC);
   ac3 = bmp085_i2c_Read_Int(fd,0xAE);
   ac4 = bmp085_i2c_Read_Int(fd,0xB0);
   ac5 = bmp085_i2c_Read_Int(fd,0xB2);
   ac6 = bmp085_i2c_Read_Int(fd,0xB4);
   b1 = bmp085_i2c_Read_Int(fd,0xB6);
   b2 = bmp085_i2c_Read_Int(fd,0xB8);
   mb = bmp085_i2c_Read_Int(fd,0xBA);
   mc = bmp085_i2c_Read_Int(fd,0xBC);
   md = bmp085_i2c_Read_Int(fd,0xBE);
   close(fd);
}

// Read the uncompensated temperature value
unsigned int BmpSensor::bmp085_ReadUT()
{
   unsigned int ut = 0;
   int fd = bmp085_i2c_Begin();

   // Write 0x2E into Register 0xF4
   // This requests a temperature reading
   bmp085_i2c_Write_Byte(fd,0xF4,0x2E);

   // Wait at least 4.5ms
   usleep(5000);

   // Read the two byte result from address 0xF6
   ut = bmp085_i2c_Read_Int(fd,0xF6);

   // Close the i2c file
   close (fd);

   return ut;
}

// Read the uncompensated pressure value
unsigned int BmpSensor::bmp085_ReadUP()
{
   unsigned int up = 0;
   int fd = bmp085_i2c_Begin();

   // Write 0x34+(BMP085_OVERSAMPLING_SETTING<<6) into register 0xF4
   // Request a pressure reading w/ oversampling setting
   bmp085_i2c_Write_Byte(fd,0xF4,0x34 + (BMP085_OVERSAMPLING_SETTING<<6));

   // Wait for conversion, delay time dependent on oversampling setting
   usleep((2 + (3<<BMP085_OVERSAMPLING_SETTING)) * 1000);

   // Read the three byte result from 0xF6
   // 0xF6 = MSB, 0xF7 = LSB and 0xF8 = XLSB
   __u8 values[3];
   bmp085_i2c_Read_Block(fd, 0xF6, 3, values);

   up = (((unsigned int) values[0] << 16) | ((unsigned int) values[1] << 8) | (unsigned int) values[2]) >> (8-BMP085_OVERSAMPLING_SETTING);
   
   close (fd);

   return up;
}

// Calculate pressure given uncalibrated pressure
// Value returned will be in units of XXXXX
unsigned int BmpSensor::bmp085_GetPressure(unsigned int up)
{
   int x1, x2, x3, b3, b6, p;
   unsigned int b4, b7;

   b6 = b5 - 4000;
   // Calculate B3
   x1 = (b2 * (b6 * b6)>>12)>>11;
   x2 = (ac2 * b6)>>11;
   x3 = x1 + x2;
   b3 = (((((int)ac1)*4 + x3)<<BMP085_OVERSAMPLING_SETTING) + 2)>>2;

   // Calculate B4
   x1 = (ac3 * b6)>>13;
   x2 = (b1 * ((b6 * b6)>>12))>>16;
   x3 = ((x1 + x2) + 2)>>2;
   b4 = (ac4 * (unsigned int)(x3 + 32768))>>15;

   b7 = ((unsigned int)(up - b3) * (50000>>BMP085_OVERSAMPLING_SETTING));
   if (b7 < 0x80000000)
      p = (b7<<1)/b4;
   else
      p = (b7/b4)<<1;

   x1 = (p>>8) * (p>>8);
   x1 = (x1 * 3038)>>16;
   x2 = (-7357 * p)>>16;
   p += (x1 + x2 + 3791)>>4;

   return p;
}

// Calculate temperature given uncalibrated temperature
// Value returned will be in units of 0.1 deg C
unsigned int BmpSensor::bmp085_GetTemperature(unsigned int ut)
{
   int x1, x2;

   x1 = (((int)ut - (int)ac6)*(int)ac5) >> 15;
   x2 = ((int)mc << 11)/(x1 + md);
   b5 = x1 + x2;

   unsigned int result = ((b5 + 8)>>4);

   return result;
}

unsigned int BmpSensor::bmp085_Altitude(double pressure, double t, double seaLevelPressure)
{
   // P = P_0 * e ^ { -\frac{Mgh}{RT} }
   
   double M = 0.029;
   double g = 9.81;
   double R = 8.31;
   double T = t + 273;
   
   return -log(pressure/seaLevelPressure) * R * T / (M * g) ;
}

BmpSensor::BmpSensor(const QString& name, int secondsPoll, QObject *parent) :
    AbstractSensor("BMP", name, secondsPoll, parent)
{
    deviceFileName = "/dev/i2c-1";
    bmp085_Calibration();
    fAltitude = 0;
    fPressure = 0;
    fTemperature = 0;
}

BmpSensor::BmpSensor(const QString &configGroupName, QObject *parent)
    :AbstractSensor(configGroupName, parent)
{
    deviceFileName = "/dev/i2c-1";
    bmp085_Calibration();
    fAltitude = 0;
    fPressure = 0;
    fTemperature = 0;}


BmpSensor::~BmpSensor()
{
}

QString BmpSensor::stringValues(const QString &format)
{
    return AbstractSensor::stringValues(format)
            .replace("%t", QString("%1").arg(temperature()))
            .replace("%p", QString("%1").arg(pressure()))
            .replace("%a", QString("%1").arg(altitude()));
}

bool BmpSensor::getNewValues()
{
    bool ok = true;
    
    try
    {
        unsigned int temperature, pressure, altitude;
        temperature = bmp085_GetTemperature(bmp085_ReadUT());
        pressure = bmp085_GetPressure(bmp085_ReadUP());
        altitude = bmp085_Altitude(pressure, temperature);

        setValues(temperature / 10.0, pressure/ 100.0, altitude);
    }
    catch(Error& err)
    {
        qDebug() << QString("fail to read Bmp085 (%1): %2").arg(deviceFileName).arg(BmpSensor::errorMessage(err));
        ok = false;
    }
    
    return ok;
}

void BmpSensor::setValues(double t, double p, double a)
{
    QVariantMap map;
    map["temperature"] = t;
    map["pressure"] = p;
    map["altitude"] = a;

    AbstractSensor::setValuesFromMap(map);
}

void BmpSensor::setTemperature(double t)
{
    double delta = fTemperature - t;
    if (fTemperature != t)
    {
        fTemperature = t;
        emit temperatureChanged(fTemperature, delta);
    }
}

void BmpSensor::setPressure(double p)
{
    double delta = fPressure - p;
    if (fPressure != p)
    {
        fPressure = p;
        emit pressureChanged(fPressure, delta);
    }
}

void BmpSensor::setAltitude(double a)
{
    double delta = fAltitude - a;
    if (fAltitude != a)
    {
        fAltitude = a;
        emit altitudeChanged(fAltitude, delta);
    }
}

QString BmpSensor::errorMessage(Error& err)
{
    switch (err)
    {
        case openError: return "Fail to open device file"; break;
        case ioctlError: return "Fail to set ioctl on file"; break;
        case readError: return "Fail to read from file"; break;
        case writeError: return "Fail to write to file"; break;
        default: return "Unknown fail"; break;
    }
}
