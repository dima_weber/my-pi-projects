#ifndef FAKESENSOR_H
#define FAKESENSOR_H

#include "abstractsensor.h"

class FakeSensor : public AbstractSensor
{
    Q_OBJECT
    Q_PROPERTY(double  power READ power WRITE setPower NOTIFY powerChanged)

    double fPower;
public:
    explicit FakeSensor(const QString& name, int secondsPoll, QObject* parent = 0);
    explicit FakeSensor(const QString& configGroupName, QObject* parent = 0);
    virtual ~FakeSensor();

    double power() const
    { return fPower; }

    virtual QString stringValues(const QString& format);

signals:
    void powerChanged(double v, double delta);

protected :
    virtual int minPollInterval() const;

protected slots:
    virtual bool getNewValues();
    void setPower(double v);
    void setValues(double map);

};

#endif // FAKESENSOR_H
