#ifndef BMPDRIVER_H
#define BMPDRIVER_H

#include "abstractsensor.h"
#include <linux/types.h>

class BmpSensor : public AbstractSensor
{
    Q_OBJECT
    Q_PROPERTY(double pressure READ pressure WRITE setPressure NOTIFY pressureChanged)
    Q_PROPERTY(double altitude READ altitude WRITE setAltitude NOTIFY altitudeChanged)
    Q_PROPERTY(double temperature READ temperature WRITE setTemperature NOTIFY temperatureChanged)
public:
    enum Error {openError, ioctlError, readError, writeError};
    static QString errorMessage(Error& );
    
    explicit BmpSensor(const QString& name, int secondsPoll, QObject *parent = 0);
    explicit BmpSensor (const QString& configGroupName, QObject* parent = 0);
    virtual ~BmpSensor();

    double temperature() const
        {return fTemperature;}
    double pressure() const
        {return fPressure;}
    double altitude () const
        {return fAltitude;}

    /// %t - temperature
    /// %p - pressure
    /// %a - altitude
    virtual QString stringValues(const QString& format);

signals:
    void temperatureChanged(double t, double delta);
    void pressureChanged(double p, double delta);
    void altitudeChanged(double a, double delta);
    
protected slots:
    virtual bool getNewValues();
    void setValues(double t, double p, double a);

private:
    QString deviceFileName;

    double fTemperature;
    double fAltitude;
    double fPressure;

    void setTemperature(double t);
    void setPressure(double p);
    void setAltitude(double a);

    // LOW LEVEL BMP085 functions
    int bmp085_i2c_Begin();
    __s32 bmp085_i2c_Read_Int(int fd, __u8 address);
    void bmp085_i2c_Write_Byte(int fd, __u8 address, __u8 value);
    void bmp085_i2c_Read_Block(int fd, __u8 address, __u8 length, __u8 *values);
    void bmp085_Calibration();
    
    unsigned int bmp085_ReadUT();
    unsigned int bmp085_ReadUP();
    unsigned int bmp085_GetPressure(unsigned int up);
    unsigned int bmp085_GetTemperature(unsigned int ut);
    unsigned int bmp085_Altitude(double pressure, double t, double seaLevelPressure = 101325.0);

    // Calibration values - These are stored in the BMP085
    short int ac1;
    short int ac2;
    short int ac3;
    unsigned short int ac4;
    unsigned short int ac5;
    unsigned short int ac6;
    short int b1;
    short int b2;
    short int mb;
    short int mc;
    short int md;

    int b5;
};

#endif // BMPDRIVER_H
