#ifndef DSSENSOR_H
#define DSSENSOR_H
#include "abstractsensor.h"

#include <QDir>
#include <QStringList>
#include <QString>
#include <QDebug>

class DsSensor : public AbstractSensor
{
    Q_OBJECT
    Q_PROPERTY(double  temperature READ temperature WRITE setTemperature NOTIFY temperatureChanged)
    Q_PROPERTY(QString w1id READ w1id)
public:
    explicit DsSensor(const QString& name, const QString& id, int secondsPoll, QObject* parent = 0);
    explicit DsSensor(const QString& configGroupName, const QString& id, QObject* parent = 0);
    virtual ~DsSensor();

    double temperature() const
		{return fTemperature;}

    QString w1id() const
        {return dsId; }

    /// %t - temperature
    virtual QString stringValues(const QString& format);

    static QStringList enumerateAvaliableSensors();
    static QList<DsSensor*> buildSensors(int secondsPoll, QObject* parent=0);
    static QList<DsSensor*> buildSensors(const QString& configGroupName, QObject* parent=0);

signals:
    void temperatureChanged(double t, double delta);

protected :
    virtual int minPollInterval() const;

protected slots:
	virtual bool getNewValues();
    void setValues(double t);

private:
    double fTemperature;

    void setTemperature(double t);
    QString dsId;
    QString dev_name;
};

#endif
