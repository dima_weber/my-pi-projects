#include "abstractsensor.h"

class DhtSensor : public AbstractSensor
{
    Q_OBJECT
    Q_PROPERTY(double  temperature READ temperature WRITE setTemperature NOTIFY temperatureChanged)
    Q_PROPERTY(double  humidity    READ humidity    WRITE setHumidity    NOTIFY humidityChanged)

public:
	enum SensorType {DHT11=11, DHT22=22, AM2302=2203};
    static SensorType DhtTypeStringToValue(const QString& value);

    DhtSensor(const QString& name, SensorType type, int pin, int secondsPoll, QObject* parent = 0);
    DhtSensor (const QString& configGroupName, QObject* parent = 0);
    virtual ~DhtSensor();
    
    double temperature() const
		{return fTemperature;}
    double humidity() const
		{return fHumidity;}

    /// %t - temperature
    /// %h - humidity
    virtual QString stringValues(const QString& format);

signals:
    void temperatureChanged(double t, double delta);
    void humidityChanged(double h, double delta);
    
protected :
    virtual int minPollInterval() const;

protected slots:
	virtual bool getNewValues();	
    void setValues(double t, double h);

private:
    double fTemperature;
    double fHumidity;

    void setTemperature(double t);
    void setHumidity(double h);
    
    void initialize(SensorType type, int pin);

	SensorType eType;
	int iPin;
    
	int data[5];
#ifdef DHT_DEBUG
	int bits[250];
	int bitidx;
#endif
};
