#include "fakesensor.h"

FakeSensor::FakeSensor(const QString &name, int secondsPoll, QObject *parent) :
    AbstractSensor("FAKE", name, secondsPoll, parent)
{
    fPower = 0;
}

FakeSensor::FakeSensor(const QString &configGroupName, QObject *parent)
    :AbstractSensor(configGroupName, parent)
{
    fPower = 0;
}

FakeSensor::~FakeSensor()
{}

QString FakeSensor::stringValues(const QString &format)
{
    return AbstractSensor::stringValues(format).replace("%v", QString::number(power(), 'f', 1));
}

int FakeSensor::minPollInterval() const
{
    return 1;
}


bool FakeSensor::getNewValues()
{
    setValues((qrand() % 1000 ) / 10.0);
	return true;
}

void FakeSensor::setPower(double v)
{
    double delta = fPower - v;
    if (fPower != v)
    {
        fPower = v;
        emit powerChanged(fPower, delta);
    }
}

void FakeSensor::setValues(double v)
{
    QVariantMap map;
    map["power"] = v;

    AbstractSensor::setValuesFromMap(map);

}

