#include "dssensor.h"

#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/fcntl.h>

#include <QRegExp>

DsSensor::DsSensor(const QString& name, const QString& id, int secondsPoll, QObject* parent)
    : AbstractSensor("DS", name, secondsPoll, parent), fTemperature(0), dsId(id)
{
    dev_name = QString("/sys/devices/w1_bus_master1/28-00000%1/w1_slave").arg(id);
}

DsSensor::DsSensor(const QString &configGroupName, const QString &id, QObject *parent)
    : AbstractSensor (configGroupName, parent), dsId (id)
{
    dev_name = QString("/sys/devices/w1_bus_master1/28-00000%1/w1_slave").arg(id);
}

DsSensor::~DsSensor()
{
}

void DsSensor::setValues(double t)
{
    QVariantMap map;
    map["temperature"] = t;

    AbstractSensor::setValuesFromMap(map);
}

void DsSensor::setTemperature(double t)
{
    double delta = fTemperature - t;
    if (fTemperature != t)
    {
        fTemperature = t;
        emit temperatureChanged(fTemperature, delta);
    }
}

bool DsSensor::getNewValues()
{
    int fileDescriptor = -1;
    int ret;
    char buffer[100];
    QString rawData;
    QRegExp rxCrcOk ("crc=[0-9a-f]{2,2} YES");
    QRegExp rxTempValue ("t=([-0-9]*)");

    bool ok = true;

    fileDescriptor = open(dev_name.toUtf8().data(), O_RDONLY);
    if (fileDescriptor < 0)
    {
#ifdef DS_DEBUG
        qDebug() << "fail to open device file";
#endif
        ok = false;
    }

    if (ok)
    {
        ret = read (fileDescriptor, buffer, sizeof(buffer));
        if (ret < 0)
        {
#ifdef DS_DEBUG
            qDebug() << "fail to read from ds " << dsId;
#endif
            ok = false;
        }
    }

    if (ok)
    {
        rawData = QString::fromUtf8(buffer);

        if (rxCrcOk.indexIn(rawData) < 0)
        {
#ifdef DS_DEBUG
            qDebug() << "crc not ok";
#endif
            ok = false;
        }
    }

    if (ok)
    {
        if (rxTempValue.indexIn(rawData) < 0)
        {
#ifdef DS_DEBUG
            qDebug() << "t=XXXXX line not found";
#endif
            ok = false;
        }
    }

    if (ok)
    {
        long value = rxTempValue.cap(1).toInt();

        setValues(value / 1000.0);
    }

    close(fileDescriptor);

    return ok;
}

QStringList DsSensor::enumerateAvaliableSensors()
{
    QStringList lst;
    QDir w1DevicesDir ("/sys/bus/w1/devices");

    foreach(QString deviceName, w1DevicesDir.entryList(QStringList() << "28-00000*"))
    {
#ifdef DS_DEBUG
        qDebug() << QString("w1 DS device %1 found").arg(deviceName);
#endif
        lst.append(deviceName.remove("28-00000"));
    }
    return lst;
}

QList<DsSensor*> DsSensor::buildSensors(int secondsPoll, QObject* parent)
{
    QList<DsSensor*> ret;

    foreach(QString id, DsSensor::enumerateAvaliableSensors())
    {
        QString name = QString("ds-%1").arg(id);
        ret.append (new DsSensor(name, id, secondsPoll, parent));
    }
    return ret;
}

QList<DsSensor *> DsSensor::buildSensors(const QString &configGroupName, QObject *parent)
{
    QList<DsSensor*> ret;

    foreach(QString id, DsSensor::enumerateAvaliableSensors())
    {
        QString name = QString("ds-%1").arg(id);
        ret.append (new DsSensor(configGroupName, id, parent));
    }
    return ret;
}

int DsSensor::minPollInterval() const
{ return 3; }

QString DsSensor::stringValues(const QString& format)
{
    return AbstractSensor::stringValues(format)
            .replace("%t", QString("%1").arg(temperature()));
}
