//  How to access GPIO registers from C-code on the Raspberry-Pi
//  Example program
//  15-January-2012
//  Dom and Gert
//


// Access from ARM Running Linux

#define BCM2708_PERI_BASE        0x20000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000) /* GPIO controller */


#include <stdlib.h>
#include <dirent.h>
#include <fcntl.h>
#include <assert.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <bcm2835.h>
#include <unistd.h>
#include "config.h"
#include "dhtsensor.h"

#define MAXTIMINGS 100

void DhtSensor::setValues(double t, double h)
{
    QVariantMap map;
    map["temperature"] = t;
    map["humidity"] = h;

    AbstractSensor::setValuesFromMap(map);
}

void DhtSensor::setTemperature(double t)
{
    double delta = fTemperature - t;
    if (fTemperature != t)
    {
        fTemperature = t;
        emit temperatureChanged(fTemperature, delta);
    }
}

void DhtSensor::setHumidity(double h)
{
    double delta = fHumidity - h;
    if (fHumidity != h)
    {
        fHumidity = h;
        emit humidityChanged(fHumidity, delta);
    }
}

void DhtSensor::initialize(DhtSensor::SensorType type, int pin)
{
    eType = type;
    iPin = pin;

    fTemperature = 0;
    fHumidity = 0;

    if (!bcm2835_init())
        throw 1;
}

DhtSensor::SensorType DhtSensor::DhtTypeStringToValue(const QString &value)
{
    if (value.compare("dht22", Qt::CaseInsensitive) == 0)
        return DHT22;
    else  if (value.compare("dht11", Qt::CaseInsensitive) == 0)
        return DHT11;
    else if (value.compare("am2302", Qt::CaseInsensitive) == 0)
        return AM2302;
    else
        return DHT22;
}

DhtSensor::DhtSensor(const QString &name, DhtSensor::SensorType type, int pin, int secondsPoll, QObject* parent)
    :AbstractSensor("DHT", name, secondsPoll, parent)
{
    initialize(type, pin);
}

DhtSensor::DhtSensor(const QString &configGroupName, QObject *parent)
    : AbstractSensor(configGroupName, parent)
{
    Config config;
    config.beginGroup(configGroupName);

    int pin = config.iValue("data_pin", 4);
    SensorType type = DhtSensor::DhtTypeStringToValue( config.sValue("type"));

    initialize(type, pin);

    config.endGroup();
}

DhtSensor::~DhtSensor()
{
    bcm2835_close();
}

QString DhtSensor::stringValues(const QString &format)
{
    return AbstractSensor::stringValues(format)
            .replace("%t", QString("%1").arg(temperature()))
            .replace("%h", QString("%1").arg(humidity()));
}

bool DhtSensor::getNewValues()
{
    bool ok = true;
    int counter = 0;
    int laststate = HIGH;
    int j=0;

    // Set GPIO pin to output
    bcm2835_gpio_fsel(iPin, BCM2835_GPIO_FSEL_OUTP);

    bcm2835_gpio_write(iPin, HIGH);
    usleep(500000);  // 500 ms
    bcm2835_gpio_write(iPin, LOW);
    usleep(20000);

    bcm2835_gpio_fsel(iPin, BCM2835_GPIO_FSEL_INPT);

    data[0] = data[1] = data[2] = data[3] = data[4] = 0;

    // wait for pin to drop?
    counter = 0;
    while (bcm2835_gpio_lev(iPin) == 1 && ++counter<1000)
        usleep(1);
    if (counter==1000)
        ok = false;

    // read data!
    if (ok)
    {
#ifdef DHT_DEBUG
        bitidx = 0;
#endif
        for (int i=0; i< MAXTIMINGS; i++)
        {
    		counter = 0;
    		while ( bcm2835_gpio_lev(iPin) == laststate && counter < 1000)
    		{
    		    counter++;
    		    //nanosleep(1);		// overclocking might change this?
    		}
    		laststate = bcm2835_gpio_lev(iPin);
    		if (counter == 1000)
    		{
                break;
    		}

#ifdef DHT_DEBUG
    	bits[bitidx++] = counter;
#endif
    		if ((i>3) && (i%2 == 0))
    		{
    		    // shove each bit into the storage bytes
    		    data[j/8] <<= 1;
    		    if (counter > 200)
    				data[j/8] |= 1;
    		    j++;
    		}
        }
    }

#ifdef DHT_DEBUG 
    /*
    if (ok)
    {
        for (int i=3; i<bitidx; i+=2)
        {
		    qDebug("bit %d: %d", i-3, bits[i]);
		    qDebug("bit %d: %d (%d)", i-2, bits[i+1], bits[i+1] > 200);
        }
        qDebug("Data (%d): 0x%x 0x%x 0x%x 0x%x 0x%x", j, data[0], data[1], data[2], data[3], data[4]);
    }
    */
#endif

    if (ok && (j >= 39) &&
		(data[4] == ((data[0] + data[1] + data[2] + data[3]) & 0xFF)) )
    {
		switch (eType)
		{
			case DHT11:
                setValues(data[2], data[0]);
			    break;
			case DHT22:
			case AM2302:
    			{
                    double t, h;
    			    h = data[0] * 256 + data[1];
    			    h /= 10;

    			    t = (data[2] & 0x7F) *256 + data[3];
    			    t /= 10.0;
    			    if (data[2] & 0x80)
    					t *= -1;

                    setValues(t, h);
    			}
		}

#ifdef DHT_DEBUG
        qDebug("Values are updated");
#endif
    }
    else
    {
#ifdef DHT_DEBUG
        qDebug ("CRC error or not enought data or not initialized");
#endif
        ok = false;
    }

    return ok;
}


int DhtSensor::minPollInterval() const
{
    return 5;
}
