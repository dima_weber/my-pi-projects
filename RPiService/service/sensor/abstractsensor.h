#ifndef DRIVER_H
#define DRIVER_H

#include <QObject>
#include <QTimer>
#include <QElapsedTimer>
#include <QDateTime>
#include <QVariantMap>

class QThread;
class AbstractSensor : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString type READ type CONSTANT FINAL)
    Q_PROPERTY(QString name READ name CONSTANT FINAL)
    Q_PROPERTY(QDateTime lastUpdate READ lastUpdateTime NOTIFY valuesUpdated FINAL)
    Q_PROPERTY(bool valid READ isValid  NOTIFY valuesUpdated FINAL)
    Q_PROPERTY(bool uptodate READ isUpToDate  NOTIFY valuesUpdated FINAL)
    Q_PROPERTY(quint64 timestamp READ timestamp  NOTIFY valuesUpdated FINAL)
    Q_PROPERTY(quint64 failreads READ failreads  STORED false FINAL)
    Q_PROPERTY(quint64 successreads READ successreads STORED false FINAL)
    Q_PROPERTY(quint64 notreadyreads READ notreadyreads STORED false FINAL) 
public:
    AbstractSensor(const QString& type, const QString& name, int secondsPoll, QObject* parent = 0);
    AbstractSensor (const QString& configGroupName, QObject* parent = 0);
    virtual ~AbstractSensor();

    bool isValid() const;
    bool isUpToDate() const;
    QDateTime lastUpdateTime() const;
    quint64 timestamp() const;
    bool polling;

    virtual QVariantMap values() const;

    /// return QString with values, formated as format
    /// %L replaced with lastUpdate string
    /// %T replaced with last update timestamp
    /// %V replaced with isValid value
    /// %U replaced with isUpToDate value
    virtual QString stringValues(const QString& format);

    QString type() const;
    QString name() const;

    quint64 failreads() const;
    quint64 successreads() const;
    quint64 notreadyreads() const;

    void setDateTimeFormat(const QString& fmt);
signals:
    void valuesUpdated(QVariantMap);
    void alarm();
    void restored();
    
public slots:
    void start();

protected :
    virtual int minPollInterval() const;

protected slots:
    virtual bool getNewValues() =0;
    void setValuesFromMap(const QVariantMap& map);

private slots:
    virtual void onTimer();

private:
    QString sensorName;
    QString sensorType;
    QDateTime lastUpdateDate;
    bool bDataSet;
    QTimer* pTimer;
    QElapsedTimer timeSinceLastPoll;
    QElapsedTimer timeSinceLastAlarm;
    QString dateTimeFormat;
    quint64 reads[3];

    void initialize (const QString& type, const QString& name, int secondsPoll);
};

#endif
