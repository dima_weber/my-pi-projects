#include "abstractsensor.h"
#include "../config.h"
#include <QDebug>
#include <QMetaProperty>

AbstractSensor::AbstractSensor(const QString &type, const QString& name, int secondsPoll, QObject* parent)
    :QObject(parent)
{
    initialize (type, name, secondsPoll);
}

AbstractSensor::AbstractSensor(const QString &configGroupName, QObject *parent)
    :QObject (parent)
{
    Config config;
    config.beginGroup(configGroupName);
    QString type = config.sValue("class");
    QString name = config.sValue("name");
    int secondsPoll = config.iValue("update_interval");

    initialize (type, name, secondsPoll);
}

AbstractSensor::~AbstractSensor()
{
    pTimer->stop();
}

bool AbstractSensor::isValid() const
{
    return bDataSet;
}

bool AbstractSensor::isUpToDate() const
{
    return lastUpdateDate.secsTo(QDateTime::currentDateTime()) < 5 * 60;
}

QDateTime AbstractSensor::lastUpdateTime() const
{return lastUpdateDate;}

quint64 AbstractSensor::failreads() const
{
    return reads[0];
}

quint64 AbstractSensor::successreads() const
{
    return reads[1];
}

quint64 AbstractSensor::notreadyreads() const
{
    return reads[2];
}

quint64 AbstractSensor::timestamp() const
{
    return lastUpdateTime().toTime_t();
}

QVariantMap AbstractSensor::values() const
{
    QVariantMap value;

    const QMetaObject *metaobject = metaObject();
    int count = metaobject->propertyCount();
    for (int i=0; i<count; ++i)
    {
        QMetaProperty metaproperty = metaobject->property(i);
        const char *name = metaproperty.name();
        if (strcmp(name, "objectName") == 0)
            continue;
        if (!metaproperty.isStored())
            continue;
        QVariant val = property(name);
        if (static_cast<QMetaType::Type>(val.type()) != QMetaType::Double)
            value[name] = val;
        else
            value[name] = QString::number(val.toDouble(), 'f', 1);
    }

    return value;
}

QString AbstractSensor::stringValues(const QString &format)
{
    return QString(format).replace("%T", QString::number(lastUpdateTime().toTime_t()))
            .replace("%L",  lastUpdateTime().toString(dateTimeFormat))
            .replace("%V", QString("%1").arg(isValid()))
            .replace("%U", QString("%1").arg(isUpToDate()));
}

QString AbstractSensor::type() const
{
    return sensorType;
}

QString AbstractSensor::name() const
{
    return sensorName;
}

void AbstractSensor::setDateTimeFormat(const QString &fmt)
{
    dateTimeFormat = fmt;
}

void AbstractSensor::start()
{
    pTimer->start();
}

int AbstractSensor::minPollInterval() const
{
    return 2;
}

void AbstractSensor::setValuesFromMap(const QVariantMap &map)
{
    foreach (QString key, map.keys())
    {
        QByteArray propName = key.toUtf8();
        setProperty(propName.constData(), map[key]);
    }

    lastUpdateDate = QDateTime::currentDateTime();
    bDataSet = true;

    emit valuesUpdated(values());
}

void AbstractSensor::onTimer()
{
#ifndef NDEBUG
    qDebug() << QString("Polling %1 started").arg(name());
#endif
    bool ok = true;
    if (polling)
    {
        qDebug() << "poll already in progress";
        ok = false;
        reads[2]++;
    }

    polling = true;
    if (timeSinceLastPoll.isValid() && !timeSinceLastPoll.hasExpired(5*1000) )
    {

        qDebug("Driver is not ready yet");
        ok = false;
        reads[2]++;
    }

    if (ok)
    {
        if (getNewValues())
        {
            timeSinceLastPoll.restart();
            
            if (timeSinceLastAlarm.isValid())
                emit restored();

            timeSinceLastAlarm.invalidate();

            reads[1]++;
        }
        else
        {
            if (timeSinceLastPoll.hasExpired(5 * 60 * 1000) )
            {
#ifndef NDEBUG
		qDebug() << QString("sensor %1 do not responce for too long").arg(name());
#endif
                if (!timeSinceLastAlarm.isValid() || timeSinceLastAlarm.hasExpired(60 * 60 * 1000))
                {
                    timeSinceLastAlarm.restart();
#ifndef NDEBUG
		    qDebug() << QString("sensor %1 still not responce -- more then hour since last remind").arg(name());
#endif
                    emit alarm();
                }
            }
            reads[0]++;
        }
    }
    polling = false;
    pTimer->start();
#ifndef NDEBUG
    qDebug() << QString("Polling %1 finished").arg(name());
#endif
}

void AbstractSensor::initialize(const QString &type, const QString &name, int secondsPoll)
{
    setDateTimeFormat("yyyy-MM-dd hh:mm:ss");
    sensorName = name;
    sensorType = type.toUpper();
    bDataSet = false;
    lastUpdateDate = QDateTime::currentDateTime();
    //timeSinceLastPoll.invalidate();
    timeSinceLastPoll.start(); // we need to start it here so first fail read do not produce alarm
    timeSinceLastAlarm.invalidate();

    int pollInterval = qMax(secondsPoll, minPollInterval()) * 1000;

    pTimer = new QTimer(this);
    connect (pTimer, SIGNAL(timeout()), SLOT(onTimer()));
    pTimer->setInterval(pollInterval);
    pTimer->setSingleShot(true);

    polling = false;

    reads[0] = reads[1] = reads[2] = 0;
}
