#include "application.h"
#ifdef SIGNAL_SUPPORT
#   include "signalhandler.hxx"
#endif

#ifdef AUTOEXIT
#   include <QTimer>
#   ifdef AUTOCRASH
#       include <QDateTime>
#   endif
#endif


Application::Application(int argc, char *argv[]) :
    QCoreApplication(argc, argv),
    pSigHandler(0)
{
#ifdef SIGNAL_SUPPORT
    SignalHandler::setup_unix_signal_handlers ();
    pSigHandler = new SignalHandler(this);
    connect(pSigHandler, SIGNAL(CtrlCPressed()), SLOT(onSigHandler()));
#endif

#ifdef AUTOEXIT
    QTimer* pShutdownTimer = new QTimer(this);
    pShutdownTimer->setInterval(1000 * 30);
    connect (pShutdownTimer, SIGNAL(timeout()), SLOT(onSigHandler()));
    pShutdownTimer->start();
#endif
}

Application::~Application()
{
    delete pSigHandler;
}

void Application::onSigHandler()
{
#ifdef AUTOCRASH
    qsrand( QDateTime::currentDateTime().toMSecsSinceEpoch());
    int c = qrand() % 10;
    if (c < 8)
    {
        int* p = 0;
        *p = 10;
    }
#endif
    quit();
}
