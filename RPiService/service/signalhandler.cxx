#include "signalhandler.hxx"
#include <QSocketNotifier>
#include <sys/socket.h>
#include <signal.h>
#include <unistd.h>
#include <QDebug>

SignalHandler::SignalHandler(QObject *parent)
         : QObject(parent)
{
    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, SignalHandler::sighupFd))
       qFatal("Couldn't create HUP socketpair");

    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, SignalHandler::sigtermFd))
       qFatal("Couldn't create TERM socketpair");

    if (::socketpair(AF_UNIX, SOCK_STREAM, 0, SignalHandler::sigintFd))
       qFatal("Couldn't create INT socketpair");

    snHup = new QSocketNotifier(SignalHandler::sighupFd[1], QSocketNotifier::Read, this);
    connect(snHup, SIGNAL(activated(int)), this, SLOT(handleSigHup()));
    snInt = new QSocketNotifier(SignalHandler::sigintFd[1], QSocketNotifier::Read, this);
    connect(snInt, SIGNAL(activated(int)), this, SLOT(handleSigInt()));
    snTerm = new QSocketNotifier(SignalHandler::sigtermFd[1], QSocketNotifier::Read, this);
    connect(snTerm, SIGNAL(activated(int)), this, SLOT(handleSigTerm()));
}

SignalHandler::~SignalHandler ()
{

}

int SignalHandler::setup_unix_signal_handlers()
{
    struct sigaction hup, term;

    hup.sa_handler = SignalHandler::hupSignalHandler;
    sigemptyset(&hup.sa_mask);
    hup.sa_flags = 0;
    hup.sa_flags |= SA_RESTART;

    if (sigaction(SIGHUP, &hup, 0) > 0)
       return 1;

    term.sa_handler = SignalHandler::termSignalHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags = 0;
    term.sa_flags |= SA_RESTART;

    if (sigaction(SIGTERM, &term, 0) > 0)
       return 2;

    term.sa_handler = SignalHandler::intSignalHandler;
    sigemptyset(&term.sa_mask);
    term.sa_flags = 0;
    term.sa_flags |= SA_RESTART;

    if (sigaction(SIGINT, &term, 0) > 0)
       return 3;

    return 0;
}

void SignalHandler::hupSignalHandler(int)
 {
     char a = 1;
     /*size_t wroteBytesCnt = */::write(sighupFd[0], &a, sizeof(a));
 }

 void SignalHandler::termSignalHandler(int)
 {
     char a = 1;
     /*size_t wroteBytesCnt = */::write(sigtermFd[0], &a, sizeof(a));
 }

 void SignalHandler::intSignalHandler(int)
 {
     char a = 1;
     /*size_t wroteBytesCnt = */::write(sigintFd[0], &a, sizeof(a));
 }

 void SignalHandler::handleSigTerm()
 {
     snTerm->setEnabled(false);
     char tmp;
     /*size_t readBytesCnt = */::read(sigtermFd[1], &tmp, sizeof(tmp));

    emit CtrlCPressed();
     // do Qt stuff

     snTerm->setEnabled(true);
 }

 void SignalHandler::handleSigInt()
 {
     snInt->setEnabled(false);
     char tmp;
     /*size_t readBytesCnt = */::read(sigintFd[1], &tmp, sizeof(tmp));

    emit CtrlCPressed();
     // do Qt stuff

     snInt->setEnabled(true);
 }


 void SignalHandler::handleSigHup()
 {
     snHup->setEnabled(false);
     char tmp;
     /*size_t readBytesCnt = */::read(sighupFd[1], &tmp, sizeof(tmp));

     // do Qt stuff
    emit CtrlCPressed();

     snHup->setEnabled(true);
 }

 int SignalHandler::sighupFd[2];
 int SignalHandler::sigtermFd[2];
 int SignalHandler::sigintFd[2];


 void SignalHandlerThread::run ()
 {
     p_handle = new SignalHandler();
     exec();
 }

 SignalHandlerThread::SignalHandlerThread(QObject *parent)
    :QThread(parent)
 {
     p_handle = NULL;
 }

 SignalHandlerThread::~SignalHandlerThread ()
 {
     delete p_handle;
 }
