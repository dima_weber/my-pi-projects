#include "lcddriver.h"
#include <string.h>
#include <cerrno>
#include <error.h>
#include <QTimer>
#include <QDebug>

LcdDriver::LcdDriver(int cols, int rows, QObject* parent)
    : QObject(parent)
{
    this->cols = cols;
    this->rows = rows;

    const int RS = 3;       //
    const int EN = 14;      //
    const int D0 = 4;       //
    const int D1 = 12;      //
    const int D2 = 13;      //
    const int D3 = 6;       //
    const int BITS=4;

    if (wiringPiSetup() < 0)
    {
        qDebug() << QString("Unable to open serial device: %1").arg(strerror (errno)) ;
    	return ;
    }

    handle = lcdInit(rows, cols, BITS, RS, EN, D0, D1, D2, D3, D0, D1, D2, D3);

    scrollTimers = new QTimer* [rows];
    scrollTexts = new QString [rows];
    scrollPos   = new int[rows];
    for(int i=0; i<rows; i++)
    {
        scrollTimers[i] = new QTimer(this);
        connect (scrollTimers[i], SIGNAL(timeout()), SLOT(onScrollTimer()));
    }
}

LcdDriver::~LcdDriver()
{
    for(int i=0; i<rows; i++)
    {
        delete scrollTimers[i];
    }
    delete [] scrollTimers;
    delete [] scrollTexts;
    delete [] scrollPos;

    //lcdFree(handle); // no such function yet -- memleak :(
}

void LcdDriver::scrollLine(int row, const QString& str, int delay, int wsCnt)
{
    scrollTexts[row] = str + QString(wsCnt, ' ');
    scrollPos[row] = 0;
    scrollTimers[row]->setInterval(delay);
    scrollTimers[row]->start();
    setPosition(0, row);
    puts(str.left(cols).toUtf8().constData());
}

void LcdDriver::pauseScroll(int row)
{
    scrollTimers[row]->stop();
}

void LcdDriver::resumeScroll(int row)
{
    scrollTimers[row]->start();
}

void LcdDriver::onScrollTimer()
{
    QTimer* pTimer = qobject_cast<QTimer*>(sender());

    int row=0;
    for(row=0; row<rows; row++)
    {
        if (scrollTimers[row] == pTimer)
            break;
    }
    int& col = ++scrollPos[row];

    setPosition(0, row);
    QString textToShow = scrollTexts[row].mid(col, cols);
    if (textToShow.length() == 0)
        col = 0;
    if (textToShow.length() < cols)
        textToShow += scrollTexts[row].left (cols - textToShow.length());
    puts(textToShow.toUtf8().constData());
}
