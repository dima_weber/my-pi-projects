#ifndef APPLICATION_H
#define APPLICATION_H

#include <QCoreApplication>

class SignalHandler;
class Application : public QCoreApplication
{
    Q_OBJECT
    SignalHandler* pSigHandler;
public:
    explicit Application(int argc, char* argv[]);
    virtual ~Application();
    
private slots:
    void onSigHandler();
};

#endif // APPLICATION_H
