#include "supervisor.h"
#include "sqlitedatabase.h"
#include "abstractsensor.h"
#include "config.h"

#ifdef MYSQL_SUPPORT
# include "mysqldatabase.h"
#endif

#ifdef DHT_SUPPORT
# include "dhtsensor.h"
#endif

#ifdef BMP_SUPPORT
# include "bmpsensor.h"
#endif

#ifdef DS_SUPPORT
# include "dssensor.h"
#endif

#ifdef FAKE_SUPPORT
# include "fakesensor.h"
#endif

#ifdef WEB_SUPPORT
# include <QxtHttpSessionManager>
# include <QxtWebContent>
# include <QxtWebServiceDirectory>
# include <QHostAddress>
# include "rootservice.h"
# include "dataservice.h"
# include "sensorwebservice.h"
#endif

#ifdef SMTP_SUPPORT
# include "emailengine.h"
#endif

#ifdef SMS_SUPPORT
# include "smsdaemon.h"
# include "smsservice.h"
#endif

#ifdef LCD_SUPPORT
# include "lcddriver.h"
#endif


#include <QThreadPool>
#include <QDateTime>
#include <QDebug>
#include <QStringList>

Supervisor::Supervisor( QObject *parent)
    :QObject(parent),
     rootService(0), pSmtpEngine(0), pSmsDaemon (0), pSmsService(0), pLcd(0)
{
    Config::initConfig("rpisettings.conf");

    readConfig();

    initLcd();
    initWeb();
    initDb();
    initSmtp();
    initSms();
    initSensors();

    qDebug() << "RpiServer Started!";
}

Supervisor::~Supervisor()
{
    emit displayClear();
    emit displayRequest(0,0, "Shutting");
    emit displayRequest(0,1, "    down");

    shutdownSensors();
    shutdownSms();
    shutdownSmtp();
    shutdownDb();
    shutdownWeb();
    shutdownLcd();

    qDebug() << "RpiServer Finished!";
}

QVariant Supervisor::status()
{
    QVariantMap map;
    int counter=0;
    foreach (AbstractSensor* pSensor, sensorList.values())
    {
        map[QString::number(counter++)] = pSensor->values();
    }
    return map;
}

void Supervisor::initSms()
{
#ifdef SMS_SUPPORT
    pSmsDaemon = new SmsDaemon(this);

    connect (this, SIGNAL(smsRequest(QString,QString)), pSmsDaemon, SLOT(sendSms(QString,QString)));

    pSmsDaemon->start();

    pSmsService = new SmsService(pSmsDaemon, pSessionManager, this);
    rootService->addService("smsstatus", pSmsService);
#endif
}

void Supervisor::initSmtp()
{
#ifdef SMTP_SUPPORT
    pSmtpEngine = new SMTPThread("email1", this);
    pSmtpEngine->start();

    connect (this, SIGNAL(emailRequest(QString)), pSmtpEngine, SLOT(addMsg(QString)));
#endif
}

void Supervisor::initLcd()
{
#ifdef LCD_SUPPORT
    pLcd = new LcdDriver(16, 2, this);
    connect(this, SIGNAL(displayRequest(int,int,QString)), pLcd, SLOT(display(int, int, QString)));
    connect(this, SIGNAL(displayClear()), pLcd, SLOT(clear()));
    pLcd->scrollLine(0, "RPiService running", 400, 3);
    pLcd->scrollLine(1, QDateTime::currentDateTime().toString(), 500, 3);
    QTimer* pLcdTimer = new QTimer(this);
    connect (pLcdTimer, SIGNAL(timeout()), SLOT(showInfo()));
    pLcdTimer->start(10 * 1000);
#endif
}

void Supervisor::initSensors()
{
    Config config;
    config.beginGroup("sensors");
    QStringList sensorEnum = config.childKeys();
    foreach (QString key, sensorEnum)
    {
        QString sensorName = config.sValue(key);
        bool enabled = config.bValue("enabled", true);
        if (enabled)
            buildAndRegisterSensorFromConfig (sensorName);
    }

    config.endGroup();
}

void Supervisor::initDb()
{
    Database* pDb = 0;
    Config config;
    config.beginGroup("databases");
    foreach(QString key, config.childKeys())
    {
        QString dbGroupName = config.sValue(key);
#ifdef MYSQL_SUPPORT
        if (dbGroupName.startsWith("mysql"))
            pDb = new MySqlDatabase(dbGroupName);
#endif

#ifdef SQLITE_SUPPORT
        if (dbGroupName.startsWith("sqlite"))
            pDb = new SqliteDatabase(dbGroupName);
#endif
        if (pDb)
        {
            pDb->connectDatabase();
            registerNewDb(pDb);
        }
    }
    config.endGroup();
}

void Supervisor::shutdownWeb()
{
#if WEB_SUPPORT
    foreach (QxtWebServiceDirectory* pService, serviceList)
    {
        delete pService;
    }
    delete rootService;
    delete pSessionManager;
#endif
}

void Supervisor::shutdownSms()
{
#ifdef SMS_SUPPORT
    delete pSmsService;

    pSmsDaemon->stop(true);
    delete pSmsDaemon;
#endif
}

void Supervisor::shutdownSmtp()
{
#ifdef SMTP_SUPPORT
    pSmtpEngine->stop(true);
    delete pSmtpEngine;
#endif
}

void Supervisor::shutdownLcd()
{
#ifdef LCD_SUPPORT
    pLcd->clear();
    delete pLcd;
#endif
}

void Supervisor::shutdownSensors()
{
    foreach (QString name, sensorList.keys())
    {
        unregisterSensor(name);
    }
    sensorList.clear();
}

void Supervisor::shutdownDb()
{
    while (!databaseList.isEmpty())
    {
        unregisterDb(databaseList.first().first->name());
    }

}

void Supervisor::readConfig()
{
    Config config;
    config.beginGroup("sms1");
    smsNumber = config.value("recipient").toString();
    alarmTemplate = config.value("alarm_message").toString();
    restoreTemplate = config.value("restore_message").toString();
    config.endGroup();
}

void Supervisor::registerNewSensor(AbstractSensor *pSensor, const QString &name, const QString &format)
{
    QThread* pThr;
    unregisterSensor(name);
    sensorList[name] = pSensor;
    pThr = new QThread(this);
    pThr->setObjectName(QString("Sensor %1 thread").arg(pSensor->name()));

    pSensor->moveToThread(pThr);

    connect (pThr, SIGNAL(started()), pSensor, SLOT(start()));

    sensorThreadList[pSensor] = pThr;
    pThr->start();

#if WEB_SUPPORT
    SensorWebService* pService = new SensorWebService(pSensor, format, pSessionManager, this);
    addService(name, pService);
#endif

    connect (pSensor, SIGNAL(alarm()), SLOT(onSensorAlarm()));
    connect (pSensor, SIGNAL(restored()), SLOT(onSensorRestored()));
    
    emit newSensorAdded(pSensor);
}

void Supervisor::buildAndRegisterSensorFromConfig(const QString &groupName)
{
    Config config;
    config.beginGroup(groupName);
    QString sensorClass = config.sValue("class");
    QString sensorMessage = config.sValue("message");
    QString sensorName = config.sValue("name");
    AbstractSensor* pSensor = 0;
    try
    {
#ifdef BMP_SUPPORT
        if (sensorClass == "bmp")
            pSensor = new  BmpSensor(groupName);
#endif
#ifdef DHT_SUPPORT
        if (sensorClass == "dht")
            pSensor = new DhtSensor(groupName);
#endif
#ifdef DS_SUPPORT
        if (sensorClass == "ds")
            pSensor = DsSensor::buildSensors(groupName)[0];
#endif
#ifdef FAKE_SUPPORT
        if (sensorClass == "fake")
            pSensor = new FakeSensor (groupName);
#endif

        if (pSensor)
            registerNewSensor(pSensor, sensorName, sensorMessage);
    }
    catch (int code)
    {
        qDebug() << QString("fail to initialize %1 sensor").arg(sensorName);
    }
#ifdef BMP_SUPPORT
    catch (BmpSensor::Error code)
    {
        qDebug() << QString("fail to initialize %1 sensor: %2").arg(sensorName).arg(BmpSensor::errorMessage(code));
    }
#endif

    config.endGroup();
}

void Supervisor::registerNewDb(Database *pDatabase)
{
    unregisterDb(pDatabase->name());

    QPair<Database*, QThread* > pair;
    pair.first = pDatabase;
    pair.second = new QThread(this);

    databaseList.append(pair);
    // register already existing sensors in DB
    foreach (AbstractSensor* pSensor, sensorList)
    {
        pDatabase->createTableForObject(pSensor);
    }
    // make future sensors register in DB
    connect(this, SIGNAL(newSensorAdded(QObject*)), pDatabase, SLOT(createTableForObject(QObject*)));

    pDatabase->moveToThread(pair.second);

    pair.second->setObjectName("db Thread");
    pair.second->start();
#ifdef WEB_SUPPORT
    QString name = QString("data-%1").arg(pDatabase->name());
    addService(name, new DataService(pDatabase, pSessionManager));
#endif
}

void Supervisor::unregisterSensor(const QString &name)
{
    QThread* pThr;
    if (sensorList.contains(name))
    {
        qDebug() << QString("Sensor %1 already exists. Reregister it (old will be deleted").arg(name);

        AbstractSensor* pPrevSensor = sensorList.take(name);
        pThr = sensorThreadList.take(pPrevSensor);
        pThr->quit();
        pThr->wait();
        delete pThr;
        delete pPrevSensor;
    }
}

void Supervisor::unregisterDb(const QString &name)
{
    Database* pDb;
    QThread* pThr;
    int i;
    for(i=0; i<databaseList.size(); i++)
    {
        pDb = databaseList[i].first;
        pThr = databaseList[i].second;
        if (pDb->name() != name)
            continue;
        else
        {
            pThr->quit();
            pThr->wait();
            delete pDb;
            delete pThr;

            databaseList.removeAt(i);

            break;
        }
    }
}

void Supervisor::addService(QString uri, QxtWebServiceDirectory *newService)
{
#if WEB_SUPPORT
    rootService->addService(uri,newService);
    serviceList.append(newService);
#endif
}

void Supervisor::onSensorAlarm()
{
    AbstractSensor* pSensor = dynamic_cast<AbstractSensor*>(sender());
    QString message = QString(alarmTemplate).replace("%s", pSensor->name());
    emit emailRequest(message);
    emit smsRequest(smsNumber, message);
}

void Supervisor::onSensorRestored()
{
    AbstractSensor* pSensor = dynamic_cast<AbstractSensor*>(sender());
    QString message = QString(restoreTemplate).replace("%s", pSensor->name());
    emit emailRequest(message);
    emit smsRequest(smsNumber, message);
}

void Supervisor::initWeb()
{
#ifdef WEB_SUPPORT
    QHostAddress host("0.0.0.0");
    int port = 18566;

    pSessionManager = new QxtHttpSessionManager(this);
    rootService = new RootService(pSessionManager, this);

    pSessionManager->setPort(port);
    pSessionManager->setListenInterface(host);
    pSessionManager->setConnector(QxtHttpSessionManager::HttpServer);
    pSessionManager->setStaticContentService(rootService);
    pSessionManager->start();
#endif
}

void Supervisor::showInfo()
{
    QString line;
    emit displayClear();

    QStringList temperaturesList;
    QStringList pressuresList;
    QStringList humiditiesList;

    foreach (AbstractSensor* pSensor, sensorList)
    {
        line = pSensor->stringValues("%t");
        if (line != "%t")
            temperaturesList.append(line);

        line = pSensor->stringValues("%p");
        if (line != "%p")
            pressuresList.append(line);

        line = pSensor->stringValues("%h");
        if (line != "%h")
            humiditiesList.append(line);
    }
    line = QString("T:%1").arg(temperaturesList.join(" "));
    emit displayRequest(0, 0, line);

    line = QString("H:%1 P:%2").arg(humiditiesList.join(" ")).arg(pressuresList.join(" "));
    emit displayRequest(0, 1, line);
}
