#ifndef CONFIG_H
#define CONFIG_H

#include <QSettings>

class Config : public QSettings
{
    static QString mFileName;
public:
    QString  sValue(const QString& key, const QString& defaultValue = QString()) const;
    int      iValue(const QString& key, int defaultValue = 0) const;
    bool     bValue(const QString& key, bool defaultValue = false) const;

    Config(QObject* parent = 0);

    static void initConfig(const QString& fileName);
};

#endif // CONFIG_H
