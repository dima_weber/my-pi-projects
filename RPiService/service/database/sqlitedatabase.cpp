#include "sqlitedatabase.h"
#include <QSqlQuery>
#include <QSqlRecord>
#include <QSqlError>
#include <QDateTime>
#include <QDebug>
#include <QFile>
#include <QStringList>
#include <QDir>
#include <QThread>
#include <QMetaProperty>
#include <QMetaObject>

SqliteDatabase::SqliteDatabase(const QString& connectionName, QObject* parent)
    :SqlDatabase(connectionName, parent)
{
}

SqliteDatabase::~SqliteDatabase()
{
}

QVariantList SqliteDatabase::getData(const QDateTime &startDT, const QDateTime &endDT, const Database::DataType& granularity, int cnt, Method method)
{
    QVariantList result;
    QString tableName = "%1Stat";
    switch (granularity)
    {
        case Raw:       tableName = tableName.arg("Raw"); break;
        case Minute:    tableName = tableName.arg("Minute"); break;
        case Hour:      tableName = tableName.arg("Hour"); break;
        case Day:       tableName = tableName.arg("Day"); break;
    }

    QStringList fieldNames;
    fieldNames << "day" << "hour" << "temp" << "hum" << "press";

    QString dataSQL = QString("SELECT %1 from %2").arg(fieldNames.join(",")).arg(tableName);
    QSqlQuery dataQuery (dataSQL, *pDatabase);
    if (!dataQuery.exec())
    {
        QString("%1 [%2] error: %3").arg(__FILE__).arg(__LINE__).arg(dataQuery.lastError().text());
    }
    else
    {
        QVariantMap element;
        QMap<QString, int> fieldIndex;
        foreach(QString field, fieldNames)
        {
            fieldIndex[field] =  dataQuery.record().indexOf(field);
        }

        while(dataQuery.next())
        {
            element.clear();

            element["date"] = QString("%1 %2").arg(dataQuery.value(fieldIndex["day"]).toString()).arg(dataQuery.value(fieldIndex["hour"]).toString());
            foreach(QString field, fieldNames)
            {
                element[field] = dataQuery.value(fieldIndex[field]);
            }
            result.append(element);
        }
    }

    return result;
}

bool SqliteDatabase::createTableForObject(QObject *pSensor)
{
    bool ok = false;
    QString sqlLine = "CREATE TABLE IF NOT EXISTS %1 (%2)";
    QString type = pSensor->property("type").toString();
    QString name = pSensor->property("name").toString();
    QString tableName = QString("SENSOR_%1").arg(type);
    QStringList fields;

    const QMetaObject *metaobject = pSensor->metaObject();
    int count = metaobject->propertyCount();
    for (int i=0; i<count; ++i)
    {
        QMetaProperty metaproperty = metaobject->property(i);
        const char *name = metaproperty.name();
        if (QString(name) == "type")
            continue;
        if (!metaproperty.isStored())
            continue;
        QString typeName = metaproperty.typeName();
        fields.append(QString("%1 %2").arg(name).arg(typeName));
    }

    QString sql;
    sql = sqlLine.arg(tableName).arg(fields.join(","));

    QSqlQuery tableCreateQuery(*pDatabase);
    ok = tableCreateQuery.exec(sql);
    if (!ok)
        qDebug() << QString("Error creating table for sensor %1: %2").arg(name).arg(tableCreateQuery.lastError().text());
    else
    {
        if (!insertQueries.contains(type))
        {
            QSqlQuery* pQuery = new QSqlQuery(*pDatabase);
            QString insertSql = insertStatementForObject(pSensor);
            ok = pQuery->prepare(insertSql);
            if (!ok)
                qDebug() << QString("Error creating table for sensor %1: %2").arg(name).arg(tableCreateQuery.lastError().text());
            else
                insertQueries[type] = pQuery;
        }
    }

    ok = SqlDatabase::createTableForObject(pSensor);

    return ok;
}

QString SqliteDatabase::insertStatementForObject(QObject *pSensor)
{
    QString sql("INSERT INTO %1 (%2) values (%3)");
    QString type = pSensor->property("type").toString();
    QString tableName = QString("SENSOR_%1").arg(type);
    QStringList fieldsList;
    QStringList valuesList;

    const QMetaObject *metaobject = pSensor->metaObject();
    int count = metaobject->propertyCount();
    for (int i=0; i<count; ++i)
    {
        QMetaProperty metaproperty = metaobject->property(i);
        const char *name = metaproperty.name();
        if (QString(name) == "type")
            continue;
        if (!metaproperty.isStored())
            continue;
        fieldsList.append(name);
        valuesList.append(QString(":%1").arg(name));
    }

    return sql.arg(tableName).arg(fieldsList.join(",")).arg(valuesList.join(","));
}

void SqliteDatabase::createDatabase()
{
    QDir dir;
    dir.mkpath(dbEnv());
    if (!QFile::exists(dbPath()))
    {
        QFile dbFile(dbPath());
        dbFile.open(QFile::WriteOnly);
        dbFile.close();
    }
}

void SqliteDatabase::createTables()
{
    if (!pDatabase->isOpen())
        qDebug() << "Database is not open, open first";
    else if (!pDatabase->isValid())
        qDebug() << "Database is invalid, recreate please";
    else
    {
        QSqlQuery createQuery(*pDatabase);
        QFile schemaFile(":/conf/schema.sql");
        if (!schemaFile.open(QFile::ReadOnly))
            qDebug() << "Fail to open schema sql";
        else
        {
            QTextStream stream(&schemaFile);
            while (!stream.atEnd())
            {
                QString sqlLine = stream.readLine();
                if (sqlLine.isEmpty())
                    continue;
                if (!createQuery.exec(sqlLine))
                {
                    qDebug() << QString("error executing %1: %2").arg(sqlLine).arg(createQuery.lastError().text());
                }
            }
        }
    }
}


void SqliteDatabase::setupConnection()
{
    pDatabase->setDatabaseName(dbPath());
}


QString SqliteDatabase::sqlDriverName()
{
    return "QSQLITE";
}

int SqliteDatabase::lostConnectionCode()
{
    return 0;
}
