#ifndef SQLDATABASE_H
#define SQLDATABASE_H

#include "database.h"
#include <QSqlDatabase>
#include <QMutex>

class QSqlQuery;
class SqlDatabase : public Database
{
    Q_OBJECT
public:
    SqlDatabase(const QString& connectionName = QString(), QObject* parent = 0);
    virtual ~SqlDatabase();

    virtual QVariantList getData(const QDateTime& startDT, const QDateTime& endDT, const DataType&  granularity, int cnt, Method method) =0;
    virtual QString insertStatementForObject(QObject *pSensor) =0;

public slots:
    virtual void writeValues(const QVariantMap&);
    virtual bool connectDatabase();
    virtual bool disconnectDatabase();
    virtual bool createTableForObject(QObject *pSensor) =0;

protected:
    QMutex dbAccessLock;
    QSqlDatabase* pDatabase;
    QMap<QString, QString> insertQueriesText;
    QMap<QString, QSqlQuery*> insertQueries;
    QList<QVariantMap> failedWritesQueue;

    virtual void setupConnection() =0;
    virtual QString sqlDriverName() = 0;
    virtual bool createInsertQueury(const QString &type, const QString& sql);
    virtual int lostConnectionCode() =0;

protected slots:
    virtual void createDatabase() =0;
    virtual void createTables() =0 ;
    virtual void onWriteFail(const QVariantMap&);
    virtual void onConnectionRestored();
};
#endif // SQLDATABASE_H
