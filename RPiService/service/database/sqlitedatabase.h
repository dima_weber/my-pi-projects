#ifndef SQLITEDATABASE_H
#define SQLITEDATABASE_H

#include "sqldatabase.h"

class SqliteDatabase : public SqlDatabase
{
    Q_OBJECT

protected slots:
    virtual void createDatabase();
    virtual void createTables() ;

public:
    SqliteDatabase(const QString& connectionName =  QString(), QObject* parent = 0);
    virtual ~SqliteDatabase();

    virtual QVariantList getData(const QDateTime& startDT, const QDateTime& endDT, const DataType&  granularity, int cnt, Method method);
    virtual QString insertStatementForObject(QObject *pSensor);

protected:
    virtual void setupConnection();
    virtual QString sqlDriverName();
    virtual int lostConnectionCode();

public slots:
    virtual bool createTableForObject(QObject *pSensor);
};

#endif // SQLITEDATABASE_H
