#ifndef DATABASE_H
#define DATABASE_H

#include <QObject>
#include <QVariantList>
#include <QDateTime>

class QDateTime;

class Database : public QObject
{
    Q_OBJECT

protected:
    enum State {Disconnected, Connecting, Connected, Disconnecting, Undefined};
    State state;
    QString connectionName;

public:
    enum DataType {Raw, Minute, Hour, Day};
    enum Method {None, Max, Min, Avg, Med, Sum};

    Database(const QString& connectionName, QObject* parent = 0);
    virtual ~Database();
    /** get Historical data for given period with given granularity and calculating with given method
     *
     * @param startDT start datetime of period for wich data requested. Default is midnight of current date;
     * @param endDT   end   datetime of period for wich data requested. Default is current datetime;
     * @param granularity   precision we want. Might be Raw (no aggregation performed), Minute, How, Day. Default is minute
     * @param cnt           count of minutes/hours/days in one aggregation. Default is 1
     * @param method        aggregation function for values. Default is Avg
     */
    virtual QVariantList getData(const QDateTime& startDT = QDateTime(QDateTime::currentDateTime().date(), QTime()), const QDateTime& endDT = QDateTime::currentDateTime(),
                                 const DataType&  granularity = Minute, int cnt = 1, Method method = Avg) = 0;

    virtual QString dbEnv();
    virtual QString dbName();
    virtual QString dbPath();

    virtual QString name();
signals:
    void dataReady(const QVariantList& );
    void writeFails(const QVariantMap& data);

    void connectionBroken();
    void connectionRestored();
    void connected();
    void disconnected();

protected slots:
    virtual void createDatabase() = 0;
    virtual void createTables() = 0;
    virtual void onWriteFail(const QVariantMap&) =0;
    virtual void onConnectionRestored() =0;
    virtual void onConnected();
    virtual void onDisconnected();

public slots:
    virtual bool createTableForObject(QObject* pSensor);
    virtual bool connectDatabase();
    virtual bool disconnectDatabase();
    virtual bool reconnectDatabase();
    virtual void writeValues(const QVariantMap& values) =0;
    virtual void asyncGetData (const QDateTime& startDT = QDateTime(QDateTime::currentDateTime().date(), QTime()), const QDateTime& endDT = QDateTime::currentDateTime(),
                               const DataType&  granularity = Minute, int cnt = 1, Method method = Avg);
};
#endif
