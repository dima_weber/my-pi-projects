#include "mysqldatabase.h"
#include "../config.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QDateTime>
#include <QDebug>
#include <QStringList>
#include <QMetaProperty>
#include <QMetaObject>

MySqlDatabase::MySqlDatabase(const QString& connectionName, QObject *parent) :
    SqlDatabase(connectionName, parent)
{
}

MySqlDatabase::~MySqlDatabase()
{
}


QString MySqlDatabase::sqlDriverName()
{
    return "QMYSQL";
}

int MySqlDatabase::lostConnectionCode()
{
    // https://dev.mysql.com/doc/refman/5.0/en/error-lost-connection.html
    return 2013;
}

void MySqlDatabase::setupConnection()
{
    Config config;
    config.beginGroup(connectionName);
    pDatabase->setHostName(config.value("host", "localhost").toString());
    pDatabase->setPort(config.value("port", 3306).toInt());
    pDatabase->setUserName(config.value("user", "root").toString());
    pDatabase->setPassword(config.value("password", "root").toString());
    pDatabase->setDatabaseName(config.value("dbname", "temperatures").toString());
    QString options;
    options = QString("MYSQL_OPT_RECONNECT=%1").arg(config.value("reconnect", true).toBool()?1:0);
    pDatabase->setConnectOptions(options);
    config.endGroup();
}


QString MySqlDatabase::insertStatementForObject(QObject *pSensor)
{
    QString sql("INSERT INTO %1 (%2) values (%3)");
    QString type = pSensor->property("type").toString();
    QString tableName = QString("SENSOR_%1").arg(type);
    QStringList fieldsList;
    QStringList valuesList;

    const QMetaObject *metaobject = pSensor->metaObject();
    int count = metaobject->propertyCount();
    for (int i=0; i<count; ++i)
    {
        QMetaProperty metaproperty = metaobject->property(i);
        const char *name = metaproperty.name();
        if (QString(name) == "type")
            continue;
        if (!metaproperty.isStored())
            continue;
        fieldsList.append(name);
        valuesList.append(QString(":%1").arg(name));
    }

    return sql.arg(tableName).arg(fieldsList.join(",")).arg(valuesList.join(","));
}


bool MySqlDatabase::createTableForObject(QObject *pSensor)
{
    bool ok = false;
    QString sqlLine = "CREATE TABLE IF NOT EXISTS %1 (%2)";
    QString type = pSensor->property("type").toString();
    QString name = pSensor->property("name").toString();
    QString tableName = QString("SENSOR_%1").arg(type);
    QStringList fields;

    const QMetaObject *metaobject = pSensor->metaObject();
    int count = metaobject->propertyCount();
    for (int i=0; i<count; ++i)
    {
        QMetaProperty metaproperty = metaobject->property(i);
        const char *name = metaproperty.name();
        if (QString(name) == "type")
            continue;
        if (!metaproperty.isStored())
            continue;
        QString typeName = "";
        switch(metaproperty.type())
        {
            case QVariant::Int:     typeName = "INTEGER"; break;
            case QVariant::String:  typeName = "VARCHAR(255)"; break;
            case QVariant::DateTime: typeName = "DATETIME"; break;
            case QVariant::Double: typeName = "DECIMAL (5,1)"; break;
            case QVariant::Bool: typeName = "BOOLEAN"; break;
            case QVariant::ULongLong: typeName = "BIGINT UNSIGNED"; break;
            default: qDebug() << "unknown type " << metaproperty.typeName(); typeName = "INTEGER"; break;
        }

        fields.append(QString("%1 %2").arg(name).arg(typeName));
    }

    QString sql;
    sql = sqlLine.arg(tableName).arg(fields.join(","));

    QSqlQuery tableCreateQuery(*pDatabase);
    ok = tableCreateQuery.exec(sql);
    if (!ok)
        qDebug() << QString("Error creating table for sensor %1: %2").arg(name).arg(tableCreateQuery.lastError().text());
    else
    {
        QString insertSql = insertStatementForObject(pSensor);
        insertQueriesText[type] = insertSql;
        ok = createInsertQueury(type, insertSql);
    }

    ok = SqlDatabase::createTableForObject(pSensor);

    return ok;
}


QVariantList MySqlDatabase::getData(const QDateTime& startDT, const QDateTime& endDT, const DataType&  granularity, int cnt, Method method)
{
    QVariantList result;
    switch (granularity)
    {
        case Database::Raw: break;
        case Database::Day: break;
        case Database::Hour: break;
        case Database::Minute: break;
    }

    return result;

    QString sqlTemplate = "SELECT %1 from %2 %3 %4";
    QStringList fields;

    QString from = "SENSOR_DHT";
    QString what;
    QString where;
    QString group;
    QString order;

    fields << "temperature" << "humidity";
    if (method == None)
        what = fields.join(",");
    else
    {
        QString func;
        switch (method)
        {
            case Min: func = "MIN (%1)"; break;
            case Max: func = "MAX (%1)"; break;
            case Avg: func = "AVG (%1)"; break;
            case Med: func = "Med (%1)"; break;
            case Sum: func = "SUM (%1)"; break;
        }
        QStringList aggregationFields;
        foreach (QString field, fields)
            aggregationFields << func.arg(field);
        what = aggregationFields.join(",");
    }

    QStringList dateRange;
    if (startDT.isValid())
        dateRange << QString("timestamp >= %1").arg( QString::number(startDT.toTime_t()) );
    if (endDT.isValid())
        dateRange << QString("timestamp <= %1").arg( QString::number(endDT.toTime_t()) );
    where = QString("WHERE %1").arg(dateRange.join(" and "));

    QString sql = sqlTemplate.arg(what, from, where, group, order);

    QSqlQuery query(*pDatabase);
    QVariantMap map;
    if (query.exec())
    {
        while (query.next())
        {
            map.clear();
//            map["timestamp"] = query.value()
        }
    }
    else
    {
        qDebug() << QString("Error: %2").arg(query.lastError().text());
    }
}


void MySqlDatabase::createTables()
{
}


void MySqlDatabase::createDatabase()
{
}
