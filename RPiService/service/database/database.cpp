#include "database.h"
#include "abstractsensor.h"

#include <QDir>
#include <QObject>
#include <qdebug.h>

Database::Database(const QString& connectionName, QObject* parent)
    :QObject(parent)
{
    connect (this, SIGNAL(connected()), SLOT(onConnected()));
    connect (this, SIGNAL(disconnected()), SLOT(onDisconnected()));
    state = Disconnected;
    this->connectionName = connectionName;
}

Database::~Database()
{}

QString Database::dbEnv()
{
    return "db";
}

QString Database::dbName()
{
    return "temperatures.db";
}

QString Database::dbPath()
{
    return QDir(dbEnv()).absoluteFilePath(dbName());
}

QString Database::name()
{
    return connectionName;
}

void Database::onConnected()
{
    state = Connected;
}

void Database::onDisconnected()
{
    state = Disconnected;
}

bool Database::connectDatabase()
{
    emit connected();
    return true;
}

bool Database::disconnectDatabase()
{
    emit disconnected();
    return true;
}

bool Database::reconnectDatabase()
{
    bool ok;
    ok = disconnectDatabase();
    if (ok)
        ok = connectDatabase();
    if (ok)
        emit connectionRestored();
    return ok;
}

void Database::asyncGetData (const QDateTime &startDT, const QDateTime &endDT, const DataType& granularity, int cnt, Method method)
{
    emit dataReady(getData(startDT, endDT, granularity, cnt, method));
}

bool Database::createTableForObject(QObject *pSensor)
{
    // absolutaly non-oop code here
    // but i don't  see how to connect valuesUpdate signal other way
    AbstractSensor* pAS = qobject_cast<AbstractSensor*>(pSensor);
    if (pAS)
        return connect (pAS, SIGNAL(valuesUpdated(QVariantMap)), this, SLOT(writeValues(QVariantMap)), Qt::QueuedConnection);
    else
    {
        qDebug() << "no automatic update signal will be send";
        return true;
    }
}
