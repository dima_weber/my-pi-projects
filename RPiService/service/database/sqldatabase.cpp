#include "sqldatabase.h"
//#include <mysql/mysql.h>
#include <QDebug>
#include <QMutexLocker>
//#include <QSqlDriver>
#include <QSqlError>
#include <QMetaProperty>
#include <QMetaObject>
#include <QSqlQuery>

SqlDatabase::SqlDatabase(const QString& connectionName, QObject *parent)
    :Database(connectionName, parent)
{
    pDatabase = 0;
    connect (this, SIGNAL(writeFails(QVariantMap)), this, SLOT(onWriteFail(QVariantMap)), Qt::QueuedConnection);
    connect (this, SIGNAL(connectionBroken()), this, SLOT(reconnectDatabase()));
    connect (this, SIGNAL(connectionRestored()), this, SLOT(onConnectionRestored()));
}

SqlDatabase::~SqlDatabase()
{
    disconnectDatabase();
}

void SqlDatabase::writeValues(const QVariantMap & map)
{
    // we need lock dbAccess here because pQuery might be deleted with reconnect method
    // while we use it

    bool ok;
    bool disconnectDetected = false;
    QString type = map["type"].toString();

    ok = dbAccessLock.tryLock();
    if (ok)
    {
        ok = state == Connected;
        if (ok)
        {
            ok = insertQueries.contains(type);
            if (ok)
            {
                QSqlQuery* pInsertQuery = insertQueries[type];
                if (pInsertQuery)
                {
                    foreach (QString key, map.keys())
                    {
                        QVariant  value = map[key];
                        if (key == "type")
                            continue;
                        pInsertQuery->bindValue(QString(":%1").arg(key), value);
                    }

                    ok = pInsertQuery->exec();
                    if (!ok)
                    {
                        QSqlError err = pInsertQuery->lastError();
                        if (err.number() == lostConnectionCode())
                        {
                            // we cannot emit signal from here becase dbAccessLock is locked and
                            // reconnect use this lock
                            disconnectDetected = true;
                        }
                        qDebug() << QString("%1 [%2] error %3: %4").arg(__FILE__).arg(__LINE__).arg(err.number()).arg(err.text());
                        qDebug() << QString("last sql was: %1").arg(pInsertQuery->executedQuery());
                        qDebug() << "with values:";
                        QMapIterator<QString, QVariant> i(pInsertQuery->boundValues());
                        while (i.hasNext()) 
                        {
                           i.next();
                           qDebug() << QString("%1: %2").arg(i.key()).arg(i.value().toString());
                        }
                    }
                }
                else
                {
                    qDebug() << "broken insertQuery for" << type;
                    ok = false;
                }
            }
            else
            {
                qDebug() << "can not find insertQuery for type" << type;
            }
        }
        else
        {
            qDebug() << "we're not connected yet.";
//            disconnectDetected = true;
        }
        dbAccessLock.unlock();
    }

    if (disconnectDetected)
        emit connectionBroken();

    if (!ok)
        emit writeFails(map);
}

bool SqlDatabase::connectDatabase()
{
    bool ok = false;
    if (state == Connecting)
        return false;
    if (state == Connected)
        return true;

    ok = dbAccessLock.tryLock();
    if (ok)
    {
        state = Connecting;
        createDatabase();
        if (QSqlDatabase::isDriverAvailable(sqlDriverName()))
        {
            pDatabase = new QSqlDatabase(QSqlDatabase::addDatabase(sqlDriverName(), name()));
            setupConnection();
            ok = pDatabase->open();
            if (ok)
            {
                createTables();
                ok = Database::connectDatabase();
            }
            else
            {
                qDebug() << QString("%1 [%2] error: %3").arg(__FILE__).arg(__LINE__).arg(pDatabase->lastError().text());
                state = Disconnected;
            }
        }
        else
        {
            qDebug() << "no driver available for " << sqlDriverName();
        }
        dbAccessLock.unlock();
    }
    return ok;
}

bool SqlDatabase::disconnectDatabase()
{
    bool ok = true;
    if (state == Disconnecting)
        return false;
    if (state == Disconnected)
        return true;

    ok = dbAccessLock.tryLock();
 //   dbAccessLock.lock();


    if (ok)
    {
        state = Disconnecting;
        foreach (QSqlQuery* pQuery, insertQueries.values())
        {
            delete pQuery;
        }
        insertQueries.clear();

        if (pDatabase->isOpen())
        {
            pDatabase->close();
            QString connectionName = pDatabase->connectionName();
            delete pDatabase;
            pDatabase = 0;
            QSqlDatabase::removeDatabase(connectionName);

        }
        ok = Database::disconnectDatabase();
        dbAccessLock.unlock();
    }
    return ok;
}

bool SqlDatabase::createTableForObject(QObject *pSensor)
{
    return Database::createTableForObject(pSensor);
}

bool SqlDatabase::createInsertQueury(const QString& type, const QString &sql)
{
    bool ok;
    QSqlQuery* pQuery = new QSqlQuery(*pDatabase);
    ok = pQuery->prepare(sql);
    if (!ok)
    {
        qDebug() << QString("Error creating insert query %1: %2").arg(sql).arg(pQuery->lastError().text());
        delete pQuery;
    }
    else
    {
        if (insertQueries.contains(type))
        {
            qDebug() << QString("Strange, insert query already exists for type %1. Replace it").arg(type);
            delete insertQueries[type];
        }
        insertQueries[type] = pQuery;
    }

    return ok;
}

void SqlDatabase::onWriteFail(const QVariantMap& data)
{
    failedWritesQueue.append(data);
}

void SqlDatabase::onConnectionRestored()
{
    dbAccessLock.lock();
    foreach (QString type, insertQueriesText.keys())
    {
        createInsertQueury(type, insertQueriesText[type]);
    }
    dbAccessLock.unlock();

    while (!failedWritesQueue.isEmpty())
    {
        QVariantMap data = failedWritesQueue.takeFirst();
        writeValues(data);
    }
}
