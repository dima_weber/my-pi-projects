CREATE TABLE IF NOT EXISTS Bmp085 (name varchar(64), date datetime, temp number, alt number, press number, id INTEGER PRIMARY KEY);
CREATE TABLE IF NOT EXISTS temperatures (name varchar(64), date datetime, temp number, hum number, id INTEGER PRIMARY KEY);
CREATE VIEW  IF NOT EXISTS DayTemp as select date(date) as day, round(avg(temp),1) as temp, round(avg(hum),1) as hum, count(temp) as cnt from temperatures group by strftime ("%Y%j", date);
CREATE VIEW  IF NOT EXISTS MinuteTemp as select date(date) as day, strftime("%H:%M", date) as time, round(avg(temp),1) as temp, round(avg(hum),1) as hum, count(temp) as cnt from temperatures group by strftime ("%Y%j%H%M", date);
CREATE VIEW IF NOT EXISTS MinutePres as select date(date) as day, strftime("%H:%M", date) as time, round(avg(press),1) as press, round(avg(temp),1) as temp, count(temp) as cnt from Bmp085 group by strftime("%Y%j%H%M", date);
CREATE VIEW IF NOT EXISTS HourPres as select date(date) as day, strftime("%H", date) as hour, round(avg(press),1) as press, round(avg(temp),1) as temp, count(temp) as cnt from Bmp085 group by strftime("%Y%j%H", date);
CREATE VIEW IF NOT EXISTS HourTemp as select date(date) as day, strftime("%H", date) as hour, round(avg(temp),1) as temp, round(avg(hum),1) as hum, count(temp) as cnt from temperatures group by strftime ("%Y%j%H", date);
CREATE VIEW IF NOT EXISTS HourStat as select p.day, p.hour, p.press, t.temp, t.hum from HourPres p left join HourTemp t on t.day=p.day and t.hour=p.hour;
CREATE VIEW IF NOT EXISTS DayPres as select date(date) as day, round(avg(press),1) as press, round(avg(temp),1) as temp, count(temp) as cnt from Bmp085 group by strftime("%Y%j", date);
CREATE VIEW IF NOT EXISTS MinuteStat as select p.day, p.time, p.press, t.temp, t.hum from MinutePres p left join MinuteTemp t on t.day=p.day and t.time=p.time;
CREATE VIEW IF NOT EXISTS DayStat as select p.day, p.press, t.temp, t.hum from DayPres p left join DayTemp t on t.day=p.day;
CREATE INDEX IF NOT EXISTS bmp_date_idx  on bmp085 (date);
CREATE INDEX IF NOT EXISTS temp_date_idx  on temperatures (date);

