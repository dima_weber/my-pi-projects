#ifndef MYSQLDATABASE_H
#define MYSQLDATABASE_H

#include "sqldatabase.h"

class MySqlDatabase : public SqlDatabase
{
    Q_OBJECT
protected slots:
    virtual void createDatabase();
    virtual void createTables() ;

public:
    MySqlDatabase(const QString& connectionName = QString(), QObject* parent = 0);
    virtual ~MySqlDatabase();

    virtual QVariantList getData(const QDateTime& startDT, const QDateTime& endDT, const DataType&  granularity, int cnt, Method method);
    virtual QString insertStatementForObject(QObject *pSensor);

public slots:
    virtual bool createTableForObject(QObject *pSensor);

protected:
    virtual void setupConnection();
    virtual QString sqlDriverName();
    virtual int lostConnectionCode();
};

#endif // MYSQLDATABASE_H
