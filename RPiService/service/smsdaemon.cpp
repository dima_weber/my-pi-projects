#include "smsdaemon.h"
#include "gsm.h"
#include <gammu-smsd.h>
#include <gammu.h>
#include <assert.h>
#include <QDebug>
#include <QThread>
#include <QTemporaryFile>
#include <QDir>

SmsDaemon::SmsDaemon(QObject *parent) :
    QThread(parent)
{
    config = 0;
    confFilename = ":/conf/smsdrc";

    QDir().mkpath("gammu");
}

QString SmsDaemon::configFilename() const
{
    return confFilename;
}

void SmsDaemon::setConfigFilename(const QString &name)
{
    confFilename = name;
}

QVariantMap SmsDaemon::status()
{
    QVariantMap responce;
    GSM_SMSDStatus smsdStatus;
    GSMError error;

    try
    {
        error = SMSD_GetStatus(config, &smsdStatus);
        if (!error)
            throw error;

        QVariantMap battery;
        battery["percent"] = smsdStatus.Charge.BatteryPercent;
        switch (smsdStatus.Charge.ChargeState)
        {
            case GSM_BatteryPowered: battery["state"] = "powered"; break;
            case GSM_BatteryConnected:battery["state"] = "connected"; break;
            case GSM_BatteryCharging:battery["state"] = "charging"; break;
            case GSM_BatteryNotConnected:battery["state"] = "notconnected"; break;
            case GSM_BatteryFull:battery["state"] = "full"; break;
            case GSM_PowerFault:battery["state"] = "fault"; break;
        }
        battery["voltage"] = smsdStatus.Charge.BatteryVoltage;
        battery["chargevoltage"] = smsdStatus.Charge.ChargeVoltage;
        battery["chargecurrent"] = smsdStatus.Charge.ChargeCurrent;
        battery["phonecurrent"] =smsdStatus.Charge.PhoneCurrent;
        battery["temperature"] = smsdStatus.Charge.BatteryTemperature;
        battery["phonetemperature"] = smsdStatus.Charge.PhoneTemperature;
        battery["capacity"] =  smsdStatus.Charge.BatteryCapacity;
        battery["type"] = smsdStatus.Charge.BatteryType;
        QVariantMap signal;
        signal["strength"] = smsdStatus.Network.SignalStrength;
        signal["percent"]  = smsdStatus.Network.SignalPercent;
        signal["biterrorrate"] = smsdStatus.Network.BitErrorRate;

        responce["version"] = smsdStatus.Version;
        responce["phoneId"] = smsdStatus.PhoneID;
        responce["client"]  = smsdStatus.Client;
        responce["battery"]  = battery;
        responce["signal"] =  signal;
        responce["recieved"] = smsdStatus.Received;
        responce["sent"] = smsdStatus.Sent;
        responce["failed"] = smsdStatus.Failed;
        responce["imei"] = smsdStatus.IMEI;

    }
    catch (GSMError& err)
    {
        qDebug() << err.text();
    }

    return responce;
}

void SmsDaemon::run()
{
    GSMError error;
    QTemporaryFile file;
    file.open();
    QFile confFile(configFilename());
    confFile.open(QFile::ReadOnly);
    file.write(confFile.readAll());
    confFile.close();
    file.close();

    QByteArray config_file = file.fileName().toUtf8();
    /*
     * We don't need gettext, but need to set locales so that
     * charset conversion works.
     */
    GSM_InitLocales(NULL);

    /* Initalize configuration with program name */
    config = SMSD_NewConfig("smsd-rpi-sensors");
    assert(config != NULL);

    qDebug() << "Starting sms daemon loop";
    try
    {
        error = SMSD_ReadConfig(config_file.constData(), config, TRUE);
        error.checkAndThrow();

        error = SMSD_MainLoop(config, FALSE, 0);
        error.checkAndThrow();
    }
    catch (GSMError& err)
    {
        qDebug() << err.text();
    }

    /* Free configuration structure */
    SMSD_FreeConfig(config);

    emit stopped();
}

void SmsDaemon::stop(bool waitForFinished)
{
//    if (thread() != QThread::currentThread())
//    {
//        QMetaObject::invokeMethod(this, "stopServer", Qt::QueuedConnection);
//        return;
//    }

    GSMError error;
    try
    {
        error = SMSD_Shutdown(config);
        error.checkAndThrow();
    }
    catch (GSMError& err)
    {
        qDebug() << err.text();
    }

    if (waitForFinished)
        wait();
}

void SmsDaemon::sendSms(const QString &number, const QString &text)
{
    if (thread()!=QThread::currentThread())
    {
        QMetaObject::invokeMethod(this,"sendSms", Qt::QueuedConnection, Q_ARG(QString, number), Q_ARG(QString, text) );
        return;
    }

    GSM_MultiSMSMessage multiSms;
    QByteArray ba;

    multiSms.Number = 1;
    GSM_SMSMessage& sms = multiSms.SMS[0];

    memset(&sms, 0, sizeof(GSM_SMSMessage));
    ba = text.toUtf8();
    EncodeUnicode(sms.Text, ba.data(), ba.size());
    ba = number.toUtf8();
    EncodeUnicode(sms.Number, ba.data(), ba.size());
    sms.PDU = SMS_Submit;
    sms.UDH.Type = UDH_NoUDH;
    sms.Coding = SMS_Coding_Default_No_Compression;
    sms.Class = 1;

    GSMError error;
    try
    {
        error = SMSD_InjectSMS(config, &multiSms, 0);
        error.checkAndThrow();
    }
    catch (GSMError& err)
    {
        qDebug() << err.text();
    }
}
