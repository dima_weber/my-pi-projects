#include "rootservice.h"
#include <QCoreApplication>
#include <QxtWebRequestEvent>
#include <QxtWebContent>
#include <QPluginLoader>
#include <QDir>
#include "servicefactory.h"
#include "supervisor.h"
#include <json.h>

#define DEFAULT_MESSAGE "<h1>RUN AWAY!</h1>"

typedef QxtWebServiceDirectory* (*CREATE_SERVICE_FUNC)();

RootService::RootService(QxtAbstractWebSessionManager * sm , QObject *parent) :
    QxtWebServiceDirectory(sm, parent)
{
    pluginsDir = QDir(qApp->applicationDirPath());

#if defined(Q_OS_WIN)
    if (pluginsDir.dirName().toLower() == "debug" || pluginsDir.dirName().toLower() == "release")
        pluginsDir.cdUp();
#elif defined(Q_OS_MAC)
    if (pluginsDir.dirName() == "MacOS")
    {
        pluginsDir.cdUp();
        pluginsDir.cdUp();
        pluginsDir.cdUp();
    }
#endif
    pluginsDir.cd("plugins");
    QStringList pathes = QCoreApplication::libraryPaths();
    pathes.prepend(pluginsDir.absolutePath());
    QCoreApplication::setLibraryPaths(pathes);
}

RootService::~RootService()
{
    foreach (QxtWebServiceDirectory* pService, loadedServices)
    {
        delete pService;
    }
    foreach (QPluginLoader* pLoader, pluginLoaders)
    {
        pLoader->unload();
        delete pLoader;
    }
}

void RootService::indexRequested(QxtWebRequestEvent *event)
{
    Supervisor* pWebServer = dynamic_cast<Supervisor*>(parent());
    QString text = QtJson::serialize( pWebServer->status());
    postEvent(new QxtWebPageEvent(event->sessionID,
                                  event->requestID,
                                  text.toUtf8()));
}

void RootService::unknownServiceRequested(QxtWebRequestEvent *event, const QString &name)
{
    if (loadService(name))
    {

        postEvent(new QxtWebRedirectEvent(event->sessionID, event->requestID, event->originalUrl.toString(), 307));
    }
    else
    {
        postEvent(new QxtWebErrorEvent(event->sessionID, event->requestID, 404, ("Service &quot;" + QString(name).replace('<', "&lt") + "&quot; not known").toUtf8()));
    }
}


bool RootService::loadService(const QString &name)
{
    bool ok = false;
    ServiceFactory* pFactory = 0;
    try
    {
        pFactory = loadPlugin<ServiceFactory>(name);
    }
    catch (const QString& err)
    {
        qDebug() << QString("fail to load %1 plugin: %2").arg(name).arg(err);
    }

    if (pFactory)
    {
        QxtWebServiceDirectory* pNewService = pFactory->createService(sessionManager(), name);
        if (pNewService)
        {
            addService(name, pNewService);
            loadedServices[name] = pNewService;
            ok = true;
        }
    }

    return ok;
}

void RootService::unloadService(const QString &name)
{
    if (loadedServices.contains(name))
    {
        delete loadedServices[name];
        loadedServices.remove(name);
    }

    if (pluginLoaders.contains(name))
    {
        pluginLoaders[name]->unload();
        delete pluginLoaders[name];
        pluginLoaders.remove(name);
    }
}

template<class FactoryInterface>
FactoryInterface* RootService::loadPlugin(const QString &name)
{
    QPluginLoader* pLoader = 0;
    if (pluginLoaders.contains(name))
        pLoader = pluginLoaders[name];
    else
    {
        QString libName = pluginsDir.absoluteFilePath(QString("lib%1.%2").arg(name)).arg("so");
        pLoader = new QPluginLoader(this);
        pLoader->setFileName(libName);
        pluginLoaders[name] = pLoader;
    }
    FactoryInterface* pFactory = dynamic_cast<FactoryInterface*>(pLoader->instance());
    if (!pFactory)
        throw pLoader->errorString();
    return pFactory;
}
