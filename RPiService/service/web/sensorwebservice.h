#ifndef WEBSERVICE_H
#define WEBSERVICE_H

#include <QxtWebServiceDirectory>

#define DEFAULT_MESSAGE "<h1>RUN AWAY!</h1>"

#include <QThread>

class AbstractSensor;

class SensorWebService : public QxtWebServiceDirectory
{

    Q_OBJECT
    AbstractSensor* pSensorDriver;
    //QThread* pSensorThread;
    QString plainTextFormat;

public:
    SensorWebService(AbstractSensor* pSensor, const QString& plainTextFormat, QxtAbstractWebSessionManager * sm , QObject * parent = 0);
    virtual ~SensorWebService();

protected:
    virtual void indexRequested (QxtWebRequestEvent *event);

private:
    QString buildResponse(QString outFormat);
};

#endif // WEBSERVICE_H
