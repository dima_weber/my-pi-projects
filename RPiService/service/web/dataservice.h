#ifndef DATASERVICE_H
#define DATASERVICE_H

#include <QxtWebServiceDirectory>

class Database;
class DataService : public QxtWebServiceDirectory
{
    Q_OBJECT
    Database* pDb;
public:
    explicit DataService(Database* pDb, QxtAbstractWebSessionManager * sm, QObject *parent = 0);
    virtual ~DataService();
protected:
    void indexRequested (QxtWebRequestEvent *event);
};

#endif // DATASERVICE_H
