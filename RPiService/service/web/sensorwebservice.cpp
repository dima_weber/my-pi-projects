#include "sensorwebservice.h"
#include "abstractsensor.h"

#include <json.h>

#include <QDebug>
#include <QHash>
#include <QUrl>

#include <QxtWebRequestEvent>
#include <QxtWebContent>
#include <QxtJSON>
#include <QVariantMap>
#if QT_VERSION >= 0x050000
# include <QUrlQuery>
#endif

void SensorWebService::indexRequested(QxtWebRequestEvent *event)
{
//    qDebug() << "Request Headers: ";
//
//    QHash<QString, QString> ::const_iterator i = event->headers.constBegin();
//
//     while (i != event->headers.constEnd()) {
//         qDebug() << i.key() << ": " << i.value();
//         ++i;
//     }


    if (event->method.compare("POST")==0) 
    {
            postEvent(new QxtWebPageEvent(event->sessionID,
                                          event->requestID,
                                          DEFAULT_MESSAGE));
        
    } 
    else if (event->method.compare("GET")==0) 
    {
        QList<QPair<QString, QString> > params =
#if QT_VERSION < 0x050000
                event->originalUrl.queryItems();
#else
                QUrlQuery(event->originalUrl).queryItems();
#endif
        QPair<QString, QString> param;
        
        QString outFormat = "json";
        
        foreach (param, params)
        {
            if (param.first.compare("t") == 0)
            {
                outFormat = param.second;    
            }
        }

        QString response = buildResponse ( outFormat );

        postEvent(new QxtWebPageEvent(event->sessionID,
                                      event->requestID,
                                      response.toUtf8()));
    }
}

QString SensorWebService::buildResponse(QString outFormat)
{
    QString body;
    if (outFormat == "json")
    {
        QVariantMap map = pSensorDriver->values();
        body = QtJson::serializeStr(map);
    }
    else if (outFormat == "plain")
    {
        body = pSensorDriver->stringValues(plainTextFormat);
    }
    return body;
}


SensorWebService::SensorWebService(AbstractSensor* pSensor, const QString& plainTextFormat, QxtAbstractWebSessionManager * sm , QObject * parent)
    :QxtWebServiceDirectory(sm,parent)
{
    this->plainTextFormat = plainTextFormat;

//    pSensorThread = new QThread(this);
//    pSensorThread->setObjectName(QString("Sensor %1 thread").arg(pSensor->name()));
    pSensorDriver = pSensor;

//    pSensorDriver->moveToThread(pSensorThread);

//    connect (pSensorThread, SIGNAL(started()), pSensorDriver, SLOT(start()));

//    pSensorThread->start();
}

SensorWebService::~SensorWebService()
{
//    pSensorThread->exit();
    
//    pSensorThread->wait();
    
//    delete pSensorThread;
}
