#ifndef ROOTSERVICE_H
#define ROOTSERVICE_H

#include <QxtWebServiceDirectory>
#include <QMap>
#include <QDir>

class QPluginLoader;
class QxtWebServiceDirectory;

class RootService : public QxtWebServiceDirectory
{
    Q_OBJECT
    QMap<QString, QxtWebServiceDirectory*> loadedServices;
    QMap<QString, QPluginLoader*> pluginLoaders;
    QDir pluginsDir ;

public slots:
    bool loadService(const QString& name);
    void unloadService(const QString& name);

public:
    explicit RootService( QxtAbstractWebSessionManager * sm ,QObject *parent = 0);
    virtual ~RootService();
    
protected:
    void indexRequested (QxtWebRequestEvent *event);
    virtual void unknownServiceRequested ( QxtWebRequestEvent * event, const QString & name ) ;

private:
    template<class FactoryInterface>
    FactoryInterface* loadPlugin(const QString& fileName);
};

#endif // ROOTSERVICE_H
