#include "dataservice.h"
#include "sqldatabase.h"
#include <json.h>
#include <QxtWebRequestEvent>
#include <QxtWebContent>

DataService::DataService(Database *pDb, QxtAbstractWebSessionManager * sm, QObject *parent) :
    QxtWebServiceDirectory(sm, parent)
{
    this->pDb = pDb;
}

DataService::~DataService()
{

}

void DataService::indexRequested(QxtWebRequestEvent *event)
{
    QDateTime endDt = QDateTime::currentDateTime();
    QDateTime startDt = QDateTime(endDt.date(), QTime());
    QVariantList data = dynamic_cast<SqlDatabase*>(pDb)->getData(startDt, endDt, Database::Hour, 1, Database::Avg);
    QVariantMap map;
    foreach(QVariant element, data)
    {
        QVariantMap m = element.toMap();
        QString date = m["date"].toString();
        m.remove("date");
        map[date] = m;
    }

    QString json = QtJson::serializeStr(map);
    postEvent(new QxtWebPageEvent(event->sessionID,
                                  event->requestID,
                                  json.toUtf8()));
}
