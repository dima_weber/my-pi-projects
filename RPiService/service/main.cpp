#include "application.h"
#include "supervisor.h"
#include <QDir>

#ifdef AUTORESTART_SUPPORT
#   include <sys/types.h>
#   include <sys/wait.h>
#   include <unistd.h>
#   include <stdio.h>
#   include <iostream>
#   include <sys/prctl.h>
#else
#   define pid_t int
#endif

int run(int argc, char *argv[])
{
    Application app(argc, argv);
    QDir::setCurrent( QCoreApplication::applicationDirPath() );

    Supervisor myServer(&app);

    return app.exec();
}

int main(int argc, char *argv[])
{
#ifdef AUTORESTART_SUPPORT
    pid_t forkPid = 0;
    pid_t waitPid = 0;
    bool restart = true;
    while (restart)
    {
        forkPid = fork();
        if (forkPid > 0)
        {
            std::cerr << "RPi Watcher started" << std::endl;
            // parent
            int status;
            do
            {
                waitPid = waitpid (forkPid, &status, 0);
                if (waitPid == -1)
                {
                    perror ("waitpid");
                    return EXIT_FAILURE;
                }

                if (WIFEXITED(status))
                {
                    std::cerr << "rpiserver normal finish with status " << WEXITSTATUS(status) << std::endl;
                    restart = false;
                }
                else if (WIFSIGNALED(status))
                {
                    std::cerr << "rpiserver killed by signal " << WTERMSIG(status) << std::endl;
                }
                else if (WIFSTOPPED(status))
                {
                    std::cerr << "rpiserver stopped by signal " << WSTOPSIG(status) << std::endl;
                }
                else if (WIFCONTINUED(status))
                {
                    std::cerr << "rpiserver continued" << std::endl;
                }
                else
                    std::cout << "rpiserver crashed -- autorestarting it" << std::endl;
            } while (!WIFEXITED(status) && !WIFSIGNALED(status));
            std::cerr << "RPi Watcher quit" << std::endl;
        }
        else if (forkPid < 0)
        {
            // fail to start child
            perror("fail to run child");
            return EXIT_FAILURE;
        }
        else if (forkPid == 0)
        {
            // child
            prctl(PR_SET_PDEATHSIG, SIGHUP);
            return run(argc, argv);
        }
    }
#else
    return run(argc, argv);
#endif
}
