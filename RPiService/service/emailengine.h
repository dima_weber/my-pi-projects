#ifndef EMAILLOGENGINE_H
#define EMAILLOGENGINE_H

#include <QThread>
#include <QMutex>
#include <QStringList>
class QxtSmtp;

class SMTPThread : public QThread
{
    Q_OBJECT

    bool connected;
    QxtSmtp* p_smtp;
    QString user;
    QString password;
    QString server;
    int port;
    bool useTls;
    bool useSsl;
    QStringList msgBuff;
    QMutex msgBuffAccessMutex;
    
    QString subject;
    QString recipient;
    QString sender;

    void setupSignals();
    void readConfig(const QString& group);
    void initialize();
public:
    SMTPThread(const QString& emailName, QObject* parent = 0);
    SMTPThread(const QString& user, const QString& password, const QString& server, int port, bool tls, bool useSsl, QObject* parent = 0);
    ~SMTPThread();
protected:
    void connectToServer();
    virtual void run();
public slots:
    void addMsg(QString msg);
    void stop(bool waitForFinished = false);
    void mailBuffer();
private slots:
    void onSMTPConnected();
    void onSMTPDisconnected();
    void smtpError(QByteArray);
    void dummySlot();
    void onMailSent(int);
    void onMailFail(int mailid, int errCode, const QByteArray& msg);
};

#endif // EMAILLOGENGINE_H
