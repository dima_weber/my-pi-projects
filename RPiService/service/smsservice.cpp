#include "smsservice.h"
#include "smsdaemon.h"

#include <QxtWebRequestEvent>
#include <QxtWebContent>
#include <json.h>

#define default_text "No smsd setup"

SmsService::SmsService( SmsDaemon* pDaemon, QxtAbstractWebSessionManager * sm ,QObject *parent ) :
    QxtWebServiceDirectory(sm, parent), pSmsDaemon(pDaemon)
{
}

SmsService::~SmsService()
{

}

void SmsService::indexRequested(QxtWebRequestEvent *event)
{
    QString text = default_text;
    if (pSmsDaemon)
    {
        text = QtJson::serialize( pSmsDaemon->status());
    }
    postEvent(new QxtWebPageEvent(event->sessionID,
                                  event->requestID,
                                  text.toUtf8()));
}
