#ifndef GSM_H
#define GSM_H

#include <gammu.h>
#include <QObject>

class GSMError {
    GSM_Error error;
protected:
    void setCode(GSM_Error err ){error = err;}
public:
    GSMError () {error=ERR_NONE;}
    GSMError (GSM_Error err){setCode(err);}
    GSMError (const GSMError& copy){setCode(copy.code());}

    GSM_Error code() const { return error;}
    QString text() const {return QString(GSM_ErrorString(code()));}

    operator bool() const {return code() == ERR_NONE;}

    bool operator == (const GSMError& other) const { return code() == other.code();}
    bool operator != (const GSMError& other) const { return code() != other.code();}

    GSMError& operator= (const GSMError& other) {setCode(other.code()); return *this;}

    void checkAndThrow(){if (error != ERR_NONE) throw error; }
};


class GSM : public QObject
{
    Q_OBJECT

    GSM_SMSC phoneSMSCenter;
    GSM_StateMachine* pStateMachine;
    GSM_Debug_Info *pDebugInfo;
protected:
    static void sendSmsCallback(GSM_StateMachine* pSM, int status, int messageRef, void* user_data);
public:
    GSM(const QString &fileName = QString());
    ~GSM();
};

#endif // GSM_H
