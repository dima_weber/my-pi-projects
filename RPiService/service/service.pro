#-------------------------------------------------
#
# Project created by QtCreator 2013-03-18T13:59:50
#
#-------------------------------------------------

QT       += core sql
QT       -= gui

CONFIG += silent

DESTDIR = ../bin
TARGET  = rpiservice
CONFIG   -= app_bundle


CONFIG += smtp sms lcd dht bmp ds fake mysql sqlite webservice signal autorestart
CONFIG -=          

CONFIG -= autoexit
CONFIG -= autocrash

TEMPLATE = app

OBJECTS_DIR=tmp
MOC_DIR=tmp
RCC_DIR=tmp

INCLUDEPATH += ../common \
               sensor

#LIBS += -L/usr/lib -lprofiler
#QMAKE_CXXFLAGS += -g -pg -O2
#QMAKE_LFLAGS += -g -pg -O2

win32 {
    CONFIG -= sms lcd dht bmp ds signal autorestart
    CONFIG += console

    CONFIG -= webservice smtp
}

autorestart {
    unix {
        DEFINES += AUTORESTART_SUPPORT
        # idea is fork process and parent monitor child exit
        # if child crash --  fork it again
    }
}

autoexit {
    DEFINES += AUTOEXIT
    autocrash {
        DEFINES += AUTOCRASH
    }
}

webservice {
    INCLUDEPATH += ../3rdpart \
                    web
    DEFINES += WEB_SUPPORT
    QT += network
    CONFIG += qxt
    QXT += web
    #LIBS += -lssl -lcrypto
    SOURCES += web/sensorwebservice.cpp \
               web/rootservice.cpp \
               web/dataservice.cpp \
               ../3rdpart/json.cpp
    HEADERS += web/sensorwebservice.h \
               web/rootservice.h \
               web/dataservice.h \
               ../3rdpart/json.h
}

mysql {
    DEFINES += MYSQL_SUPPORT
    SOURCES += database/mysqldatabase.cpp
    HEADERS += database/mysqldatabase.h
    win32 {
        LIBS += -L"C:/Program Files/MySQL/MySQL Server 5.6/lib" -llibmysql
    }
    unix {
        LIBS += -lmysqlclient
    }
}

sqlite {
    DEFINES += SQLITE_SUPPORT
    SOURCES += database/sqlitedatabase.cpp
    HEADERS += database/sqlitedatabase.h
}

mysql|sqlite {
    SOURCES += database/database.cpp \
               database/sqldatabase.cpp
    HEADERS += database/database.h \
               database/sqldatabase.h
    INCLUDEPATH += database
}

smtp {
    DEFINES += SMTP_SUPPORT
    CONFIG += qxt
    QXT += network
    SOURCES += emailengine.cpp
    HEADERS += emailengine.h
}

sms {
    LIBS    += -L/opt/gammu/lib -lgsmsd -lGammu
    INCLUDEPATH += /opt/gammu/include/gammu \
                   /usr/include/gammu
    DEFINES += SMS_SUPPORT
    SOURCES += smsdaemon.cpp \
               smsservice.cpp \
               gsm.cpp
    HEADERS += smsdaemon.h \
               smsservice.h \
               gsm.h
}

lcd {
    LIBS    += -lwiringPiDev -lwiringPi
    DEFINES += LCD_SUPPORT
    SOURCES += lcddriver.cpp
    HEADERS += lcddriver.h
}

dht {
# http://www.airspayce.com/mikem/bcm2835/
    DEFINES += DHT_SUPPORT
    LIBS += -L/opt/bcm2835/lib -lbcm2835
    LIBS += -lrt -L/usr/lib/x86_64-linux-gnu
    INCLUDEPATH += /opt/bcm2835/include
    SOURCES += sensor/dhtsensor.cxx
    HEADERS += sensor/dhtsensor.h
}

bmp {
    INCLUDEPATH += ../3rdpart
    DEFINES += BMP_SUPPORT
    SOURCES += sensor/bmpsensor.cpp \
               ../3rdpart/smbus.c
    HEADERS += sensor/bmpsensor.h \
               ../3rdpart/smbus.h
}


ds {
    DEFINES += DS_SUPPORT
    SOURCES += sensor/dssensor.cpp
    HEADERS += sensor/dssensor.h
}

fake {
    DEFINES += FAKE_SUPPORT
    SOURCES += sensor/fakesensor.cpp
    HEADERS += sensor/fakesensor.h
}

signal {
    unix {
        DEFINES += SIGNAL_SUPPORT
        SOURCES += signalhandler.cxx
        HEADERS += signalhandler.hxx
    }
}

SOURCES += main.cpp \
    application.cpp \
    sensor/abstractsensor.cpp \
    config.cpp \
    supervisor.cpp

HEADERS += \
    ../common/servicefactory.h \
    application.h \
    sensor/abstractsensor.h \
    config.h \
    supervisor.h

RESOURCES += \
    resource.qrc

OTHER_FILES += \
    temperature.sql \
    smsdrc \
    rpisettings.conf
