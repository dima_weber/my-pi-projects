#ifndef SMSDAEMON_H
#define SMSDAEMON_H

#include <QObject>
#include <QThread>
#include <gammu.h>
#include <QVariant>
extern "C" {
    #include <gammu-smsd.h>
}

class SmsDaemon : public QThread
{
    Q_OBJECT
    GSM_SMSDConfig *config;
    QString confFilename;

public:
    explicit SmsDaemon(QObject *parent = 0);

    QString configFilename() const;
    void setConfigFilename(const QString& name);

    QVariantMap status();
signals:
    void stopped();

protected:
    virtual  void run();

public slots:
    void stop(bool waitForFinished = false);

    void sendSms(const QString& number, const QString& text );
};

#endif // SMSDAEMON_H
