#include "narodmonclient.h"
#include <QNetworkInterface>

narodmonclient::narodmonclient(QObject *parent) :
    QObject(parent)
{
}

void narodmonclient::send( const QVariantMap& data)
{
    QString narodmonHost = "narodmon.ru";
    int narodmonPort     = 8283;

    QString dataToSend;
    dataToSend = "#" + getMacAddress() + "\n";
    foreach(QVariant sensorData, data.values())
    {
        QVariantMap map = sensorData.toMap();
        dataToSend += "#" + map["id"].toString() + "#" + map["value"].toString();
        if (map.contains("time"))
            dataToSend += "#" + map["time"].toString();
        if (map.contains("name"))
            dataToSend += "#" + map["name"].toString();
        dataToSend += "\n";
    }
    dataToSend += "##";

    socket.connectToHost(narodmonHost, narodmonPort);

    if (socket.waitForConnected())
    {
        socket.write(dataToSend.toUtf8());
        QString ans = QString::fromUtf8(socket.readAll());
        socket.close();
        if (ans != "OK")
            qDebug() << ans;
    }
    else
    {
        qDebug() << QString("fail to connect %1:%2").arg(narodmonHost).arg(narodmonPort);
    }
}

QString narodmonclient::getMacAddress() const
{
    foreach(QNetworkInterface netInterface, QNetworkInterface::allInterfaces())
    {
        // Return only the first non-loopback MAC Address
        if (!(netInterface.flags() & QNetworkInterface::IsLoopBack))
            return netInterface.hardwareAddress();
    }
    return QString();
}

QVariantMap narodmonclient::getData()
{
    QVariantMap map;

    return map;
}
