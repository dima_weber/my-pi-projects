#include "gsm.h"

void GSM::sendSmsCallback(GSM_StateMachine *pSM, int status, int messageRef, void *user_data)
{

}

GSM::GSM(const QString& fileName)
{
    GSM_InitLocales(NULL);

    pDebugInfo = GSM_GetGlobalDebug();
    GSM_SetDebugFileDescriptor(stderr, TRUE, pDebugInfo);
    GSM_SetDebugLevel("textall", pDebugInfo);

    pStateMachine = GSM_AllocStateMachine();
    pDebugInfo = GSM_GetDebug(pStateMachine);
    GSM_SetDebugGlobal(false, pDebugInfo);
    GSM_SetDebugFileDescriptor(stderr, TRUE, pDebugInfo);
    GSM_SetDebugLevel("textall", pDebugInfo);

    INI_Section *config;
    GSMError error;
    error = GSM_FindGammuRC(&config, fileName.isEmpty() ? fileName.toUtf8().data() : NULL);
    error.checkAndThrow();

    error = GSM_ReadConfig(config, GSM_GetConfig(pStateMachine, 0), 0);
    error.checkAndThrow();

    INI_Free(config);

    GSM_SetConfigNum(pStateMachine, 1);

    error = GSM_InitConnection(pStateMachine, 1);
    error.checkAndThrow();

    GSM_SetSendSMSStatusCallback(pStateMachine, GSM::sendSmsCallback, 0);

}


GSM::~GSM()
{

}
