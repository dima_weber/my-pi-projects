#ifndef WEBSERVER_H
#define WEBSERVER_H

#include <QMutex>
#include <QThread>
#include <QMap>
#include <QPair>

class AbstractSensor;
class Database;
class QxtWebServiceDirectory;
class QxtHttpSessionManager;
class SMTPThread;
class SmsDaemon;
class SmsdService;
class SmsService;
class LcdDriver;

class Supervisor : public QObject
{
    Q_OBJECT
    QMap<QString, AbstractSensor*> sensorList;
    QMap<AbstractSensor*, QThread*> sensorThreadList;

    QMap<QString, QString> plainTextFormats;

    QxtHttpSessionManager* pSessionManager;
    QList<QxtWebServiceDirectory*> serviceList;
    QList< QPair<Database*, QThread* > > databaseList;

    QxtWebServiceDirectory* rootService;
    SMTPThread* pSmtpEngine;
    SmsDaemon* pSmsDaemon;
    SmsService* pSmsService;
    LcdDriver* pLcd;

    QString alarmTemplate;
    QString restoreTemplate;
    QString smsNumber;

public:
    Supervisor(QObject* parent=0);
    virtual ~Supervisor();

    QVariant status();

public slots:
    void addService(QString uri, QxtWebServiceDirectory *newService);

    void onSensorAlarm();
    void onSensorRestored();

signals:
    void postReceived(QByteArray content);
    void newSensorAdded(QObject*);

    void smsRequest(const QString& number, const QString& message);
    void emailRequest(const QString& message);

    void displayRequest (int row, int col, QString text);
    void displayClear();

protected:
    void initWeb();
    void initSms();
    void initSmtp();
    void initLcd();
    void initSensors();
    void initDb();

    void shutdownWeb();
    void shutdownSms();
    void shutdownSmtp();
    void shutdownLcd();
    void shutdownSensors();
    void shutdownDb();

    void readConfig();

protected slots:
    void showInfo();

private:
    void registerNewSensor(AbstractSensor* pSensor, const QString& name, const QString& format);
    void buildAndRegisterSensorFromConfig (const QString& name);
    void registerNewDb (Database* pDatabase);
    void unregisterSensor (const QString& name);
    void unregisterDb(const QString& name);
};

#endif // WEBSERVER_H
