#include "config.h"
#include <QFile>
#include <QDir>
#include <QDebug>

QString Config::mFileName;

QString Config::sValue(const QString& key, const QString& defaultValue) const
{
    return value(key, defaultValue).toString();
}

int Config::iValue(const QString& key, int defaultValue) const
{
    return value(key, defaultValue).toInt();
}

bool Config::bValue(const QString& key, bool defaultValue) const
{
    return value(key, defaultValue).toBool();
}


Config::Config(QObject* parent)
    :QSettings(mFileName, QSettings::IniFormat, parent)
{
    if (mFileName.isEmpty())
    {
        throw QString("call initConfig first");
    }
}

void Config::initConfig(const QString &fileName)
{
    QDir dir = QDir::current();
    mFileName = dir.absoluteFilePath(fileName);

    QFile settingsFile(mFileName);
    if (!settingsFile.exists())
    {
        qDebug() << QString("No settings file found, copy from resources to %1. Edit it").arg(mFileName);
        if (!QFile::copy(":/conf/rpisettings.conf", mFileName))
            qDebug() << "Fail to copy settings file";
    }
}
