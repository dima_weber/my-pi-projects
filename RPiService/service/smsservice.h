#ifndef SMSDSERVICE_H
#define SMSDSERVICE_H

#include <QxtWebServiceDirectory>

class SmsDaemon;
class SmsService : public QxtWebServiceDirectory
{
    Q_OBJECT
public:
    explicit SmsService( SmsDaemon* pDaemon, QxtAbstractWebSessionManager * sm ,QObject *parent = 0);
    virtual ~SmsService();

    SmsDaemon* pSmsDaemon;

protected:
    void indexRequested (QxtWebRequestEvent *event);
};

#endif // SMSDSERVICE_H
