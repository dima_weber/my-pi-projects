#ifndef NARODMODCLIENT_H
#define NARODMODCLIENT_H

#include <QObject>
#include <QTcpSocket>

class narodmonclient : public QObject
{
    Q_OBJECT
    QTcpSocket socket;
public:
    explicit narodmonclient(QObject *parent = 0);

    void send(const QVariantMap &data);
signals:

protected:
    QString getMacAddress() const;
    QVariantMap getData();
};

#endif // NARODMODCLIENT_H
