#ifndef SERVICEFACTORY_H
#define SERVICEFACTORY_H

#include <QtPlugin>

class QxtWebServiceDirectory;
class QxtAbstractWebSessionManager;

class ServiceFactory
{
public:
    virtual QxtWebServiceDirectory* createService(QxtAbstractWebSessionManager* sm, const QString& name) =0;
};

Q_DECLARE_INTERFACE(ServiceFactory, "ServiceFactory")

#endif // SERVICEFACTORY_H
