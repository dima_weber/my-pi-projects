#ifndef UPTIMESERVICE_H
#define UPTIMESERVICE_H

#include <QxtWebServiceDirectory>
#include <QElapsedTimer>

class UptimeService : public QxtWebServiceDirectory
{
    Q_OBJECT
    QElapsedTimer timer;
public:
    explicit UptimeService(QxtAbstractWebSessionManager * sm , QObject * parent = 0);
    virtual ~UptimeService();

protected:
    virtual void indexRequested (QxtWebRequestEvent *event);
};

#endif // UPTIMESERVICE_H
