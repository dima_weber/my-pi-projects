#-------------------------------------------------
#
# Project created by QtCreator 2013-07-23T18:53:04
#
#-------------------------------------------------

QT       -= gui
CONFIG += qxt
QXT += web network

TARGET    = $$qtLibraryTarget(uptime)
DESTDIR   = ../bin/plugins
TEMPLATE  = lib
CONFIG   += plugin

OBJECTS_DIR=tmp
MOC_DIR=tmp
RCC_DIR=tmp

DEFINES += UPTIME_LIBRARY

SOURCES += uptime.cpp \
    uptimeservice.cpp \
    ../3rdpart/json.cpp

HEADERS += uptime.h \
    uptimeservice.h \
    ../common/servicefactory.h \
    ../3rdpart/json.h

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target
}
