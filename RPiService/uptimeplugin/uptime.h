#ifndef UPTIME_H
#define UPTIME_H

#include <QObject>
#include "../common/servicefactory.h"

class QxtWebServiceDirectory;

class Uptime : public QObject, public ServiceFactory
{
    Q_OBJECT
#if QT_VERSION >= 0x050000
    Q_PLUGIN_METADATA(IID "Uptime" )
#endif
    Q_INTERFACES(ServiceFactory)
public:
    Uptime(QObject* parent = 0);
    virtual QxtWebServiceDirectory* createService(QxtAbstractWebSessionManager* sm, const QString &name);
};

#endif // UPTIME_H
