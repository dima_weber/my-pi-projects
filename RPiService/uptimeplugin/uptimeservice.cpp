#include "uptimeservice.h"
#include "../3rdpart/json.h"
#include <QTime>
#include <QxtWebPageEvent>

UptimeService::UptimeService(QxtAbstractWebSessionManager *sm, QObject *parent) :
    QxtWebServiceDirectory(sm, parent)
{
    timer.start();
}

UptimeService::~UptimeService()
{
}

void UptimeService::indexRequested(QxtWebRequestEvent *event)
{
    QVariantMap map;
    QTime dt;
    dt = dt.addMSecs(timer.elapsed());
    map["uptime"] = dt.toString();
    QString json = QtJson::serializeStr(map);
    postEvent(new QxtWebPageEvent(event->sessionID,
                                  event->requestID,
                                  json.toUtf8()));
}
