#include "uptime.h"
#include "uptimeservice.h"

Uptime::Uptime(QObject *parent)
    :QObject(parent)
{
}

QxtWebServiceDirectory *Uptime::createService(QxtAbstractWebSessionManager *sm, const QString &name)
{
    Q_UNUSED(name)
    return new UptimeService(sm, this);
}

#include <QtPlugin>
#if QT_VERSION < 0x050000
Q_EXPORT_PLUGIN2(update, Uptime)
#endif
