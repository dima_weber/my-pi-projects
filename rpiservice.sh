#! /bin/sh

### BEGIN INIT INFO
# Provides:          rpiservice
# Required-Start:    $all
# Required-Stop:     $all
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: starts the rpiservice server
# Description:       starts the rpiservice using start-stop-daemon
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/local/bin/rpiservice
PIDFILE=/tmp/rpiservice.pid
NAME=rpiservice
DESC="RPi Service"
USER=root

START_STOP_DAEMON=`which start-stop-daemon`

test -x $DAEMON || exit 0

set -e

start_service() {
    echo -n "Starting $DESC: "
    if [ -z $START_STOP_DAEMON ]; then
        # RedHat/CentOS
         sudo -u $USER nohup $DAEMON > /dev/null  2>dev/null &
    else
        # Debian/Ubuntu
        $START_STOP_DAEMON --start --background -c $USER  --exec $DAEMON --make-pidfile --pidfile $PIDFILE
    fi
    echo "done."
}

stop_service () {
    echo -n "Stopping $DESC: "
    if [ -z $START_STOP_DAEMON ]; then
        sudo -u xmg killall  --user $USER  $DAEMON
    else
        $START_STOP_DAEMON --stop --user $USER --exec $DAEMON  --retry TERM/5/KILL/5 --pidfile $PIDFILE
    fi
    echo "done."
}

case "$1" in
  start)
        start_service
        ;;
  stop)
        stop_service
        ;;
  restart|force-reload)
        stop_service
        start_service
        ;;
  *)
        N=/etc/init.d/$NAME
        echo "Usage: $N {start|stop|restart|force-reload}" >&2
        exit 1
        ;;
esac

exit 0
