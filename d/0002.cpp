#include <fstream>

int main(int argc, char* argv[])
{
    std::ifstream inFile;
    std::ofstream outFile;
    long n;
    inFile.open("INPUT.TXT");
    inFile >> n;
    outFile.open("OUTPUT.TXT");
    long s = n  / 10;
    s = s * (s+1);
    if (s>0)
    outFile <<  s;
    outFile << 25;

    return 0;
}
