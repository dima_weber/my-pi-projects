#include <fstream>

int main(int argc, char* argv[])
{
    std::ifstream inFile;
    std::ofstream outFile;
    int a, b;
    inFile.open("INPUT.TXT");
    inFile >> a >> b;
    outFile.open("OUTPUT.TXT");
    outFile << a+b;

    return 0;
}
