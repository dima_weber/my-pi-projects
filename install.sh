#!/bin/sh

ln -sf `readlink -f RPiService/bin/rpiservice` /usr/local/bin
cp rpiservice.sh /etc/init.d/rpiservice
chmod +x /etc/init.d/rpiservice
/usr/sbin/update-rc.d rpiservice defaults 40
